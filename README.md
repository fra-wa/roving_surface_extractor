# Roving Surface Extractor

# Table of Contents:

---

1. [**Background**](#background)
2. [**Models**](#archi)
3. [**Usage**](#usage)
    - [Installation](#install)
    - [Start the extraction](#inference)
4. [**Citation**](#citation)

---
<a name="background"></a>
# Background

This project is part of the [CRC/TRR 280](https://www.sfbtrr280.de/), principles of a new approach to construction.

To study the inner structure of new material-minimized carbon reinforced concrete structures, we use computer tomography. 
This helps us to extract and analyze the carbon roving contained within. However, simply creating a 3D reconstruction isn't 
sufficient for fully investigating the carbon rovings in the specimen (SITGrid 044 VL (Kneitz)). Our goal is to represent 
the surface using NURBS, so we need to segment and extract the rovings. Unfortunately, basic thresholding methods aren't 
sufficient for the task.

Hence, this project utilizes a [3D U-Net](https://arxiv.org/pdf/1606.06650.pdf)
(also added: [DenseVoxNet](https://doi.org/10.48550/arXiv.1708.00573) (updated 2023)) to segment the rovings. 
During inference, the volume is divided into multiple subvolumes since it's unlikely that the entire volume fits into 
the VRAM for processing. These subvolumes overlap with each other, utilizing a 3D Gaussian bell weighting strategy to 
ensure smooth segmentation of the carbon roving (updated 2023). Additionally, an automatic extraction of the roving is 
conducted to represent it as a point cloud, a binary volume, a surface, and as a vector field of normals.

| 3D extraction result using the roving surface extractor |
|---|
| <sub>This is a frame of the video: example_segmentation_tpd03_t6_c2.avi</sub>|
| <img src="/readme_files/example_segmentation_video_frame.PNG" alt="video_frame" width="800"/> |

---
<a name="archi"></a>
# Models

The 3D U-Net used is a slightly customized version of the implementation by 
[wolny](https://github.com/wolny/pytorch-3dunet). 

Update as of 2023: Added a new checkpoint: the 3D U-Net V2, which was trained utilizing the supercomputer at TUD Dresden University of 
Technology. This is currently the best traned network. The training volume size was 128x128x64. Training involved 100 epochs using the 
[AiSeg project](https://gitlab.com/fra-wa/aiseg), incorporating rotated versions of the 
[Concrete Pores Segmentation Dataset](https://doi.org/10.34740/kaggle/ds/2921245) along with online augmentation, as detailed in the study 
"[Analysis of Thin Carbon Reinforced Concrete Structures through Microtomography and Machine Learning](https://doi.org/10.3390/buildings13092399)". 
Additionally, a pretrained [DenseVoxNet](https://doi.org/10.48550/arXiv.1708.00573) has been added, which was slightly modified version derived 
from the [Medical Zoo project](https://github.com/black0017/MedicalZooPytorch). This model, despite having only 1.7 million parameters, 
outperforms the first trained UNet (V1). As a result, the checkpoint size is reduced to 20MB, making it suitable for deployment on 
less powerful machines.

<details>
<summary><b><u>More supported networks</u></b></summary>
If the segmentation is not good enough, you can also test other networks: one was trained using strong
offline data augmentation and another using online augmentation. Those networks have learned more general features and
may perform better on other Carbon Grids than SITGrid 044 VL (Kneitz).
</details>

<details>
<summary><b><u>Old 3D U-Net - removed</u></b></summary>
This is deprecated and performs worse than the newest models and was therefore replaced.

It has 16,3 million parameters and its checkpoint file has a size of 
191 MB. It was trained on a A6000 with 48GB VRAM on a handmade dataset containing 1,15 GB of data representing two 
different carbon rovings. Furthermore, 3D augmentation methods were used to increase the training data size to 43,8 GB 
containing subvolumes of size 256x256x20. In total, the model was trained for 100 epochs taking over 700 hours
(around one month).
</details>

| 2D slice                                                                            | 2D slice with manual segmentation (V1)                                                       |
|-------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|
| <img src="/readme_files/example_dataset_image.PNG" alt="dataset_img" width="400"/>  | <img src="/readme_files/example_training_dataset_image.PNG" alt="dataset_mask" width="400"/> |

| 3D view of one roving segmented for the training (V1)                                    |
|------------------------------------------------------------------------------------------|
| <img src="/readme_files/example_training_dataset_3d.PNG" alt="dataset_img" width="400"/> |

### Results:

The 3D U-Net (2023) achieved a DICE of 98.6%, the DenseVoxNet achieved 98.1%.
See at [evaluation](/evaluation) for more info.

Check out the video `example_segmentation_tpd03_t6_c2.avi` to get an impression of the extraction (3D U-Net).

---

<a name="usage"></a>
# Usage

<a name="install"></a>
## Installation
Nvidia Graphics Card with Cuda >= 11.8 required.
Verify: open a terminal, type `nvidia-smi`


**Windows:**
1. Install Python 3.9 (Download here: https://www.python.org/ftp/python/3.9.13/python-3.9.13-embed-amd64.zip)
2. execute the `windows_installer.bat` (this creates a virtual environment inside this project)

**Uninstall?** Simply delete the project.

**Linux**
1. Install python 3.9
2. [Create a virtual python environment](https://docs.python.org/3/tutorial/venv.html) and activate it
3. cd to `../roving_surface_extractor/` and call `python install_requirements.py` to install all needed packages and to 
   get the correct pytorch library.

<a name="inference"></a>
## Start the UI
**Windows**
1. execute the `start_surface_extractor.bat`

**Linux**
1. cd to `...../roving_surface_extractor/`
2. activate the virtual environment: `source virtual_env/bin/activate`
3. call `python manage.py migrate`
4. call `python manage.py runserver 127.0.0.1:8000`
5. Open a browser and go to `http://127.0.0.1:8000/`

<a name="citation"></a>
# Citation
If you use this code or the results for your research, please provide the link!
Contact: [Franz Wagner, TU Dresden](https://tu-dresden.de/bu/umwelt/geo/ipf/photogrammetrie/die-professur/beschaeftigte/m-sc-franz-wagner?set_language=en)


### Related Papers
```
@article{MesterWagner3DSeg,
  author  = {Mester, Leonie and Wagner, Franz and Liebold, Frank and Klarmann, Simon and Maas, Hans-Gerd and Klinkel, Sven},
  title   = {Image-based modeling and analysis of carbon-fibre reinforced concrete shell structures},
  comment = {IMAGE-BASED MODELLING AND ANALYSIS OF CARBON-FIBRE REINFORCED CONCRETE SHELL STRUCTURES},
  year    = {2022},
  month   = oct,
  pages   = {1631--1640},
  volume  = {6},
  issn    = {2617-4820},
  journal = {Concrete Innovation for Sustainability},
}

@Article{Wagner2023_CRC_Analysis,
  author          = {Wagner, Franz and Mester, Leonie and Klinkel, Sven and Maas, Hans-Gerd},
  title           = {Analysis of Thin Carbon Reinforced Concrete Structures through Microtomography and Machine Learning},
  journal         = {Buildings},
  volume          = {13},
  year            = {2023},
  number          = {9},
  article-number  = {2399},
  url             = {https://www.mdpi.com/2075-5309/13/9/2399},
  issn            = {2075-5309},
  doi             = {10.3390/buildings13092399}
}
```
