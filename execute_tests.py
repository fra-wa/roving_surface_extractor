import coverage
import os
import sys
import unittest


if __name__ == '__main__':
    current_dir = os.path.dirname(os.path.abspath(__file__))
    sys.path.extend([current_dir])

    coverage_dir = os.path.join(current_dir, 'test_coverage')

    cov = coverage.Coverage(
        data_file=None,
        source=[
            current_dir,
        ],
        omit=[
            '*test_*',
            '*execute_tests.py',
            '*start_extraction.py',
            '*__init__.py'
        ]
    )
    cov.start()

    testsuite = unittest.TestLoader().discover(start_dir=os.path.join(current_dir, 'dataset_creation/tests'))
    result = unittest.TextTestRunner(verbosity=1).run(testsuite)

    cov.stop()
    cov.save()
    if not os.path.exists(coverage_dir):
        os.makedirs(coverage_dir)
    cov.html_report(directory=coverage_dir)

    if not result.errors and not result.failures:
        print('Congratulations, your tests passed :)')
