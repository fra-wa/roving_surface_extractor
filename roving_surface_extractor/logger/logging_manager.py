import logging
import logging.config
import logging.handlers
import multiprocessing
import os
import threading

from .logging_events import LoggerCreatorEvent, LogRecordEvent, LoggerRemoverEvent


class LoggingHandler(logging.handlers.QueueHandler):

    def __init__(self, events_queue, logger_name):
        logging.handlers.QueueHandler.__init__(self, events_queue)
        self.logger_name = logger_name

    def enqueue(self, record):
        self.queue.put(LogRecordEvent(record, self.logger_name))


class LoggingSubprocessInitializer:

    def __init__(self, events_queue, logger_name, logging_level):
        self.events_queue = events_queue
        self.logger_name = logger_name
        self.logging_level = logging_level

    def set_up_root_logger(self):
        logger = logging.getLogger()
        handler = LoggingHandler(self.events_queue, self.logger_name)
        handler.setFormatter(logging.Formatter('%(message)s'))  # Don't format the message, it will be done later
        logger.addHandler(handler)
        logger.setLevel(self.logging_level)
        logger.propagate = False

    def remove_logger_handlers(self):
        event = LoggerRemoverEvent(self.logger_name)
        self.events_queue.put(event)


class LoggingManager:

    def __init__(self):
        self.events_queue = multiprocessing.Manager().Queue(-1)
        self.logging_running = False

    def run_logging_listener_thread(self):
        if not self.logging_running:  # avoid running more than 1 thread for logging
            self.__run_logging_listener_thread()
            self.logging_running = True

    def create_logger(self, log_name='root', log_handler=None, log_format=None, log_level=None):
        logger_creator_event = LoggerCreatorEvent(
            log_name=log_name,
            log_handler=log_handler,
            log_format=log_format,
            log_level=log_level,
        )
        self.events_queue.put(logger_creator_event)

        return LoggingSubprocessInitializer(self.events_queue,
                                            logger_creator_event.logger_name,
                                            logger_creator_event.logging_level)

    def __run_logging_listener_thread(self):
        def listener_logger_thread(events_queue):
            while True:
                event = events_queue.get()
                if event is None:
                    break
                event.execute()

        log_thread = threading.Thread(target=listener_logger_thread, args=(self.events_queue,))
        log_thread.setName(f'listenerLoggerThread_{os.getpid()}')
        log_thread.start()

    def stop_logging(self):
        if self.logging_running:
            self.events_queue.put(None)
            self.logging_running = False
