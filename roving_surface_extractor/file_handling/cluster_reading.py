import cv2
import logging
import numpy as np

from .. import constants
from ..file_handling.utils import get_files
from ..multicore_utils import slice_multicore_parts, multiprocess_function


def load_images(file_paths, order):
    if not file_paths:
        return None, None
    first_img = cv2.imread(file_paths[0], cv2.IMREAD_GRAYSCALE)
    convert = True if min(np.unique(first_img)) > 0 else False
    h, w = first_img.shape[:2]
    sub_volume = np.zeros((len(file_paths), h, w), dtype=np.uint8)
    sub_volume[0] = first_img[:]
    file_paths.pop(0)
    # if images were saved to color, we need to revert it.

    for i, file_path in enumerate(file_paths):
        img = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)
        sub_volume[i + 1] = img[:, :]

    if convert:
        sub_volume[sub_volume == 50] = 0
        sub_volume[sub_volume != 0] = 255
    return sub_volume, order


def load_gray_volume(volume_folder, logger=None):
    logging.info('Loading segmented binary volume for extraction.')
    files = get_files(volume_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
    sliced_data_map = slice_multicore_parts(files)
    order_map = [i for i in range(len(sliced_data_map))]

    results = multiprocess_function(load_images, [sliced_data_map, order_map], logger)
    logging.info('Loaded. Creating volume.')
    results = sorted(results, key=lambda x: x[1])

    volume = results[0][0]
    results.pop(0)

    for i, (sub_volume, order) in enumerate(results):
        volume = np.vstack((volume, sub_volume))
        results[i] = None  # reduces tmp memory footprint
    logging.info('Volume created.')

    return volume
