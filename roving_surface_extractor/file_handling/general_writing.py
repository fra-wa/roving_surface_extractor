import cv2
import logging
import numpy as np
import os
import pandas as pd
import sys

from .utils import get_overwritten_file_path, create_folder
from .. import constants
from ..multicore_utils import slice_volume_equally, multiprocess_function


def create_csv_string(element, fmt):
    if isinstance(element, (str, int, bool)) or element is None or element == np.nan:
        csv_string = '{}, '.format(element)
    else:
        csv_string = f'{element:{fmt}}, '
    return csv_string


def create_csv_line(line, fmt):
    """
    Dynamically creates a csv line based on the input type (text float whatever).
    Applies the given format if type of element is not int or string.

    Returns: csv line
    """
    csv_line = ''
    if isinstance(line, np.ndarray):
        line = list(line)

    for element in line:
        if type(element) == list:
            for sub_element in element:
                if type(sub_element) == list:
                    raise ValueError('Dimension of array or list is higher than 2. Please specify data.\n'
                                     'May use save_numpy_array_to_binary() and the related reading.')
                csv_line += create_csv_string(sub_element, fmt)
        else:
            csv_line += create_csv_string(element, fmt)

    csv_line = csv_line.rstrip(', ')

    csv_line += '\n'
    return csv_line


def save_csv(file_path, data, header, overwrite=True, number_format='.3f'):
    """
    Args:
        data: list or numpy array
        file_path: path to save to
        header: pass a header for your file describing the columns (string, separate by comma)
        overwrite: default True, set to false, then the current datetime will be added to the file name
        number_format: specify format of numbers
    """

    if not overwrite:
        file_path = get_overwritten_file_path(file_path)

    if type(data) == np.ndarray:
        # writing a np array is faster using pandas
        header = header.replace(' ', '').split(',')
        data_frame = pd.DataFrame(data)
        data_frame.to_csv(file_path, header=header, sep=',', float_format=number_format, index=False)
        return

    csv_to_save = '{}\n'.format(header)
    for idx, line_or_element in enumerate(data):
        try:
            csv_to_save += create_csv_line(line_or_element, number_format)
        except TypeError:
            # only 1D
            csv_to_save += create_csv_string(line_or_element, number_format)
            if idx + 1 == len(data):
                csv_to_save = csv_to_save.rstrip(', ')

    with open(file_path, 'w') as f:
        f.write(csv_to_save)


def array_to_string(array, format_string=None, separator=','):
    if format_string is None:
        format_string = ':0.3f'

    # special case to handle space separators
    separate_with_spaces = False
    spaced_separators = [i * ' ' for i in range(1, 10)]
    if separator in spaced_separators:
        multiplier = [i for i in range(1, 10)]
        index = spaced_separators.index(separator)
        separator = multiplier[index] * ';'
        separate_with_spaces = True

    formatter = {'float_kind': ('{' + f'{format_string}' + '}').format}

    with np.printoptions(suppress=True, formatter=formatter, threshold=sys.maxsize):
        text = np.array2string(array, separator=separator)

    if separate_with_spaces:
        return text.replace(' ', '').replace('[', '').replace(f']{separator}', '').replace(']', '').replace(';', ' ')

    return text.replace(' ', '').replace('[', '').replace(f']{separator}', '').replace(']', '')


def get_ply_data_string(vertices, faces, normals=None, min_precision=None):
    data_string = ''
    format_string = None

    if min_precision > 3:
        format_string = f':0.{min_precision}f'

    if normals is None:
        data_string += array_to_string(vertices, separator=' ', format_string=format_string)
    else:
        format_string = ':0.5f'
        if min_precision > 5:
            format_string = f':0.{min_precision}f'
        vertices_and_normals = np.hstack((vertices, normals))
        data_string += array_to_string(vertices_and_normals, format_string=format_string, separator=' ')

    data_string += '\n' + array_to_string(faces, separator=' ', format_string=format_string)

    return data_string


def save_surface_ply(file_path, vertices, faces, normals=None, overwrite=True, min_precision=3):
    """
    ascii saver for ply surface. Consider using Trimesh package (google for: pypi trimesh)!

    Args:
        file_path: path to save to
        vertices:
        faces:
        normals:
        overwrite: default True, set to false, then the current datetime will be added to the file name
        min_precision: int, formats array values like: ':0.3f' however, if defaults have higher precision like the
            normals default (:0.5f), the default is used!
    """

    if not file_path.endswith('.ply'):
        raise ValueError('File type must be a .ply!')

    if not overwrite:
        file_path = get_overwritten_file_path(file_path)

    header = [
        'ply',
        'format ascii 1.0',
        f'element vertex {vertices.shape[0]}',
        'property float x',
        'property float y',
        'property float z',
    ]

    if normals is not None:
        header += [
            'property float nx',
            'property float ny',
            'property float nz',
        ]

    header += [
        f'element faces {faces.shape[0]}',
        'property list uchar int vertex_indices',
        'end_header',
    ]

    faces_with_uchar = np.full((faces.shape[0], 4), 3)
    faces_with_uchar[:, 1:] = faces

    header_string = '\n'.join(header)
    data_string = get_ply_data_string(vertices, faces_with_uchar, normals, min_precision)
    text_to_save = header_string + '\n' + data_string

    with open(file_path, 'w') as f:
        f.write(text_to_save)


def save_coordinates_binary_ply(file_path, coordinates):
    """
    binary saver for ply coordinates.

    Args:
        file_path: path to save to
        coordinates
    """

    if not file_path.endswith('.ply'):
        raise ValueError('File type must be a .ply!')

    header = [
        b'ply',
        b'format binary_little_endian 1.0',
        b'element coordinate %d' % coordinates.shape[0],
        b'property float x',
        b'property float y',
        b'property float z',
        b'end_header',
    ]

    byte_coordinates = b''
    header_string = b'\n'.join(header)
    text_to_save = header_string + b'\n' + byte_coordinates

    with open(file_path, 'wb') as f:
        f.write(text_to_save)


def save_numpy_array_to_binary(array, file_path):
    """
    Saves any numpy array to a binary file
    Args:
        array: np.ndarray object
        file_path: file path to save the volume, extension must be .npy
    """
    if not file_path.endswith('.npy'):

        old_basename = os.path.basename(file_path)
        basename = old_basename.split('.')[0] + '.npy'
        logging.warning(f'Wrong file extension! File changed from {old_basename} to {basename}.')
        basename = basename.split('.')[0] + '.npy'
        file_path = os.path.join(os.path.dirname(file_path), basename)
    np.save(file_path, array)


def __multi_save_volume_images(sub_volume, start_index, max_slices, folder, extension='.png'):
    zero_fills = len(str(max_slices))
    if not extension.startswith('.'):
        extension = f'.{extension}'
    extension = extension.lower()
    extension = extension.lower()

    if extension not in constants.SUPPORTED_INPUT_IMAGE_TYPES:
        raise ValueError(
            f'{extension} is not in supported extensions: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}'
        )

    for z in range(sub_volume.shape[0]):
        current_id = z + start_index
        img = sub_volume[z]
        file_nr = str(current_id).zfill(zero_fills)
        file_name = f'slice_{file_nr}{extension}'
        cv2.imwrite(os.path.join(folder, file_name), img)


def save_volume(volume, folder, multiprocessing=True, extension='.png', start_index=0, max_slices=None):
    """

    Args:
        volume:
        folder:
        multiprocessing: using multicore processing to save - use this for larger volumes!
        extension:
        start_index: starts naming using this index
        max_slices: If given, zero padding will be adjusted by this number

    Returns:

    """
    create_folder(folder)

    if not extension.startswith('.'):
        extension = f'.{extension}'
    extension = extension.lower()

    if extension not in constants.SUPPORTED_INPUT_IMAGE_TYPES:
        raise ValueError(
            f'{extension} is not in supported extensions: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}'
        )

    if max_slices is None:
        max_slices = volume.shape[0]

    if multiprocessing:
        volumes_map = slice_volume_equally(volume)
        depth_map = [vol.shape[0] for vol in volumes_map]
        indexes_map = [start_index]
        for depth in depth_map[:-1]:
            indexes_map.append(indexes_map[-1] + depth)
        max_slices_map = len(volumes_map) * [max_slices]
        folders_map = len(volumes_map) * [folder]
        extension_map = len(volumes_map) * [extension]
        maps = [volumes_map, indexes_map, max_slices_map, folders_map, extension_map]
        multiprocess_function(__multi_save_volume_images, maps)
    else:
        __multi_save_volume_images(volume, start_index, max_slices, folder, extension=extension)
