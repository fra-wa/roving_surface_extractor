import csv
import numpy as np
import scipy.io as sio
import trimesh

from .utils import get_files


def convert_line_to_list(line):
    """
    Assuming that the line is a list of strings, this tries to convert numbers back to float and let strings be strings.
    """
    line_list = []
    for element in line:
        # remove whitespace
        element = element.lstrip()

        try:
            if '.' in element:
                element = float(element)
            else:
                element = int(element)
        except ValueError:
            pass

        line_list.append(element)

    return line_list


def read_csv_to_list(path, ignore_header=True):
    """
    You can pass a txt as well. But the values must be separated by a comma!
    """

    csv_list = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for line in csv_reader:
            line_list = convert_line_to_list(line)
            if line_list:
                csv_list.append(line_list)

    if ignore_header:
        csv_list.pop(0)

    return csv_list


def read_csv_to_array(path, delimiter=', ', dtype=None, encoding="utf8", skip_header=1):
    """
    Args:
        path: path of csv file
        delimiter: default ", "
        dtype: numpy dtype
        encoding: default "utf8"
        skip_header: default 1, set to 0 to keep first line

    Returns: np array

    """
    return np.genfromtxt(path, delimiter=delimiter, dtype=dtype, encoding=encoding, skip_header=skip_header)


def read_mat_file(path):
    """
    Reads .mat local_files in a given folder (if encountering some problems at decoding, add a flag for the version)

    Returns: list with file data
    """
    try:
        return sio.loadmat(path)
    except NotImplementedError:
        raise NotImplementedError('Matlabs file format is v7.3, try this: '
                                  'https://stackoverflow.com/questions/17316880/reading-v-7-3-mat-file-in-python')


def read_mat_files_in_folder(folder):
    """
    Reads .mat local_files in a given folder
    Returns: list containing mat data
    """
    mat_files = get_files(folder, extension='.mat')
    data = []

    for file in mat_files:
        data.append(read_mat_file(file))

    return data


def read_csv_files_in_folder(folder, delimiter=', ', dtype=None, encoding="utf8", skip_header=1):
    """
    Reads .csv local_files in a given folder

    path: path of csv file
        delimiter: default ", "
        dtype: numpy dtype
        encoding: default "utf8"
        skip_header: default 1, set to 0 to keep first line

    Returns: list of np arrays
    """
    csv_files = get_files(folder, extension='.csv')
    data = []

    for file in csv_files:
        data.append(read_csv_to_array(file, delimiter, dtype, encoding, skip_header))

    return data


def read_binary_to_numpy_array(file_path):
    """
    Reads a numpy ndarray saved as binary back to np.ndarray

    Args:
        file_path:

    Returns:

    """
    return np.load(file_path)


def read_ply_file(file_path):
    mesh = trimesh.load(file_path)
    return mesh
