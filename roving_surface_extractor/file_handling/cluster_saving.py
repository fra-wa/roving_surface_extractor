import cv2
import numpy as np
import os
import logging
from .general_writing import save_csv
from .utils import create_folder


def get_file_number(current_index, leading_zeroes=4):
    return str(current_index).zfill(leading_zeroes)


def save_d_h_w_coordinates_as_x_y_z(coordinates, file_path):
    x_y_z_coordinates = np.fliplr(coordinates)
    save_csv(file_path, x_y_z_coordinates, 'x, y, z', number_format='.0f')


def save_d_h_w_coordinates_as_x_y_z_as_binary(coordinates, file_path):
    title = 'Points'
    with open(file_path, 'wb') as f:
        # % HEADER
        f.write(b"# vtk DataFile Version 2.0\n")
        f.write(title.encode())
        f.write(b"\nBINARY\nDATASET UNSTRUCTURED_GRID\n\n")
        f.write(b"POINTS %d float\n" % coordinates.shape[0])
        for k in range(coordinates.shape[0]):
            percent = (k+1)/coordinates.shape[0]
            # https://numpy.org/devdocs/user/basics.byteswapping.html
            pp1 = np.array([coordinates[k, 2], coordinates[k, 1], coordinates[k, 0]], dtype=np.int16)
            pp1b = pp1.byteswap().newbyteorder()
            f.write(pp1b.tobytes())
            logging.info(f'Progress: {percent:.1f}%')


def save_volume_slices(volume, folder):
    depth, height, width = volume.shape[:3]
    create_folder(folder)

    for z in range(depth):
        img = volume[z]
        file_nr = get_file_number(z)
        file_name = 'slice_' + file_nr + '.png'
        file_path = os.path.join(folder, file_name)
        cv2.imwrite(file_path, img)


def save_binary_vector_field(vertices, normals, title, filename):
    """Code borrowed from Frank Liebold, TUD (frank.liebold@tu-dresden.de)"""
    with open(filename, 'wb') as f:
        # % HEADER
        f.write(b"# vtk DataFile Version 2.0\n")
        f.write(title.encode())
        f.write(b"\nBINARY\nDATASET UNSTRUCTURED_GRID\n\n")
        f.write(b"POINTS %d float\n" % vertices.shape[0])
        for k in range(vertices.shape[0]):
            # https://numpy.org/devdocs/user/basics.byteswapping.html
            pp1 = vertices[k].astype(np.float32)
            pp1b = pp1.byteswap().newbyteorder()
            f.write(pp1b.tobytes())

        f.write(b"\nPOINT_DATA %d\n" % vertices.shape[0])
        f.write(b"VECTORS vectors float\n")
        for k in range(normals.shape[0]):
            pp1 = normals[k].astype(np.float32)
            pp1b = pp1.byteswap().newbyteorder()
            f.write(pp1b.tobytes())
