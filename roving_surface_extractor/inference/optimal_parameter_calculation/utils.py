import cv2
import gc
import logging

import torch
from torch.utils.data import DataLoader
from torch.utils.data.dataset import Dataset

from roving_surface_extractor import constants
from roving_surface_extractor.dl_models.loading import get_model
from roving_surface_extractor.file_handling.utils import get_files
from roving_surface_extractor.multicore_utils import slice_multicore_parts, multiprocess_function, get_max_process_count


class ModelLoader:
    """Helper for usability --> easier usage of returned parameters"""

    def __init__(self, architecture, input_channels, classes, device, normalization):
        self.architecture = architecture
        self.input_channels = input_channels
        self.classes = classes
        self.device = device
        self.normalization = normalization

        self.model = None
        self.optimizer = None
        self.loss = None
        self.in_size = None

    def setup_model_optimizer_loss(self):
        model = get_model(
            model_name=self.architecture,
            channels=self.input_channels,
            classes=self.classes,
            device=self.device,
            normalization=self.normalization,
        )
        model.train()
        model.to(self.device)
        self.model = model

        self.optimizer = torch.optim.Adam(self.model.parameters())

        self.loss = torch.nn.BCEWithLogitsLoss().to(
            self.device) if self.classes == 2 else torch.nn.CrossEntropyLoss().to(self.device)

        return self.model, self.optimizer, self.loss

    def wipe_memory(self):
        self._optimizer_to(torch.device('cpu'))
        self.loss.cpu()
        self.model.cpu()
        del self.optimizer
        del self.model
        del self.loss
        gc.collect()
        torch.cuda.empty_cache()

    def _optimizer_to(self, device):
        for param in self.optimizer.state.values():
            if isinstance(param, torch.Tensor):
                param.data = param.data.to(device)
                if param._grad is not None:
                    param._grad.data = param._grad.data.to(device)
            elif isinstance(param, dict):
                for subparam in param.values():
                    if isinstance(subparam, torch.Tensor):
                        subparam.data = subparam.data.to(device)
                        if subparam._grad is not None:
                            subparam._grad.data = subparam._grad.data.to(device)


class Dataset3DSimulator(Dataset):
    """
    Dataset for volumes or time sequences
    """

    def __init__(self,
                 num_samples,
                 input_size,
                 input_depth,
                 channels,
                 load_c_d_h_w=True,  # true for volumes, false for sequences
                 ):
        if channels not in [1, 3]:
            raise ValueError(f'Channels must be 1 or 3, not {channels}!')
        self.channels = channels
        self.volumes_count = num_samples
        self.in_size = input_size
        self.in_depth = input_depth
        self.channels = channels
        self.load_C_D_H_W = load_c_d_h_w

    def __getitem__(self, item):
        if self.load_C_D_H_W:
            input_volume = torch.ones((self.channels, self.in_depth, self.in_size, self.in_size), dtype=torch.float)
        else:
            # [D, H, W, C] --> [F, C, H, W]
            input_volume = torch.ones((self.in_depth, self.channels, self.in_size, self.in_size), dtype=torch.float)
        target_volume = torch.zeros((self.in_depth, self.in_size, self.in_size), dtype=torch.float)

        return input_volume, target_volume

    def __len__(self):
        return self.volumes_count


def multi_get_min_image_size(image_paths, process_nr):
    in_size_choices = constants.IN_SIZE_CHOICES_3D
    min_size = max([in_size_choice[0] for in_size_choice in in_size_choices])

    for i, path in enumerate(image_paths):
        if process_nr == 0:
            logging.info(f'Process {process_nr + 1}: {i + 1}/{len(image_paths)}')
        h, w = cv2.imread(path, -1).shape[:2]
        min_image_dim = min([h, w])
        if min_size > min_image_dim:
            min_size = min_image_dim

    return min_size


def get_hardware_aware_num_workers(prefetch_factor, total_batches):
    """
    This function calculates the optimal num workers based on your hardware.
    Goal are (number physical cores - 1) workers.

    When to use multiple workers:
        - Your batch contains thousands of files to be loaded
        - Your batch needs to be preprocessed (e.g. augmented)

    Otherwise, using multiple workers might slow down the training!
    This is due to the fact, that each worker starts a new thread which takes a short time and needs to send
    the loaded batch back to the main thread.

    Args:
        prefetch_factor (int): prefetch factor of the batches to load. Needed for ram approximation
        total_batches (int): total number of training batches
    """

    usable_cores, total_cpu_cores = get_max_process_count(logical=False)
    max_workers = usable_cores - 1

    if max_workers <= 1:
        return max_workers

    while max_workers * prefetch_factor > total_batches:
        max_workers = max_workers - 1

    if max_workers > constants.MAX_NUM_WORKERS:
        max_workers = constants.MAX_NUM_WORKERS

    return max_workers


def calculate_num_workers(batch_size, dataset, prefetch_factor):
    """Helper for: get_num_workers_and_prefetch. This is not intended to be used anywhere else."""
    total_batches = len(dataset) // batch_size
    num_workers = get_hardware_aware_num_workers(
        prefetch_factor=prefetch_factor,
        total_batches=total_batches,
    )
    return num_workers


def get_num_workers_and_prefetch(batch_size,
                                 train_dataset,
                                 valid_dataset,
                                 max_workers=None,
                                 ):
    """
    Calculates the number of workers used for the training dataset and validation dataset based on the batch size and
    input dimensions or if online augmentation is active.
    """
    prefetch_factor = 2  # pytorch default, used with two workers is fastest without online aug
    valid_prefetch_factor = 2

    num_workers = calculate_num_workers(
        batch_size, train_dataset, prefetch_factor
    )
    num_validation_workers = calculate_num_workers(
        batch_size, valid_dataset, valid_prefetch_factor
    )

    if max_workers is not None:
        if num_workers > max_workers:
            num_workers = max_workers
        if num_validation_workers > max_workers:
            num_validation_workers = max_workers

    # using 2 workers is beneficial for larger batch sizes, 3 is slower, 4 is ok as well but 2 was the fastest!
    # 1 is also a little faster but prefetch has to be higher
    # set 2 workers has to be validated!
    if num_workers > 4:
        num_workers = 4
    elif num_workers == 1:
        prefetch_factor = 4

    # validation behaves as inference
    if num_validation_workers > 4:
        num_validation_workers = 4
    elif num_workers == 1:
        valid_prefetch_factor = 4
    return num_workers, num_validation_workers, prefetch_factor, valid_prefetch_factor


def get_max_inference_in_size(data_folder, logger):
    """
    During inference, images might be enlarged -> this is bad if it can be avoided.
    This function checks all input images and gets the minimum dimension

    Args:
        data_folder:
        logger:
    """
    logging.info('Checking images to get best inference parameters.')

    image_paths = get_files(data_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
    if not image_paths:
        raise FileNotFoundError(
            f'No files found. Images must be of type: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}.'
        )
    in_sizes = [in_choice[0] for in_choice in constants.IN_SIZE_CHOICES_3D]

    max_in_depth = None
    max_in_size = None

    if len(image_paths) > 40:
        images_map = slice_multicore_parts(image_paths)
        process_nr_map = list(range(len(images_map)))

        min_sizes = multiprocess_function(
            multi_get_min_image_size, [images_map, process_nr_map], logger=logger
        )
        min_size = min(min_sizes)
    else:
        min_size = multi_get_min_image_size(image_paths, 1)

    for in_size in in_sizes:
        if min_size >= in_size:
            max_in_size = in_size
        else:
            break

    volume_depth = len(image_paths)
    depths = [in_choice[0] for in_choice in constants.DEPTH_CHOICES_3D]
    for depth in depths:
        if volume_depth >= depth:
            max_in_depth = depth
        else:
            break

    if max_in_depth is None:
        raise ValueError(
            f'The volume contains too less slices: at least {min(depths)} slices are needed, got {len(image_paths)}'
            f'\nImages must be of type: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}.'
        )
    if max_in_depth is not None:
        logging.info(f'Max possible input size: {max_in_size}; max possible input depth: {max_in_depth}.')
    else:
        logging.info(f'Max possible input size: {max_in_size}.')
        logging.info('Tip: Larger input sizes are better! Resize your images if needed.')

    return max_in_size, max_in_depth


def get_data_loader(num_samples,
                    batch_size,
                    channels,
                    input_size,
                    input_depth=None,
                    custom_num_workers=None,
                    ):

    dataset = Dataset3DSimulator(num_samples, input_size, input_depth, channels)

    num_workers, _, prefetch_factor, _ = get_num_workers_and_prefetch(
        batch_size=batch_size,
        train_dataset=dataset,
        valid_dataset=dataset,
    )

    if custom_num_workers is not None:
        num_workers = custom_num_workers
    if num_workers == 0:
        prefetch_factor = 2

    data_loader = DataLoader(
        dataset=dataset,
        batch_size=batch_size,
        shuffle=False,
        drop_last=False,
        num_workers=num_workers,
        prefetch_factor=prefetch_factor,
    )
    return data_loader
