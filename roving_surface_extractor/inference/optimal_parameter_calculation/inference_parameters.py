import logging
import os

import torch

from roving_surface_extractor import constants
from roving_surface_extractor.utils import get_memory
from roving_surface_extractor.multicore_utils import multiprocess_function
from .utils import get_data_loader, ModelLoader
from ...file_handling.general_reading import read_csv_to_list
from ...file_handling.general_writing import save_csv


def try_inference_loop_threaded(model_loader,
                                batch_size,
                                input_size,
                                input_depth=None,
                                ):
    model, optimizer, loss_func = model_loader.setup_model_optimizer_loss()

    device = model_loader.device
    ram_used, ram_total, _ = get_memory(device)
    channels = model.in_channels

    model.eval()
    # ram usage with workers is stable after 3 iterations
    iterations = 4
    num_samples = iterations * batch_size
    data_loader = get_data_loader(num_samples, batch_size, channels, input_size, input_depth)
    with torch.no_grad():
        with torch.cuda.amp.autocast(enabled=constants.AMP_ENABLED):
            try:
                for i, (input_batch, target_batch) in enumerate(data_loader):
                    input_batch = input_batch.to(device)
                    out_logits = model(input_batch)
                    out_logits = out_logits.float().cpu().numpy()
                    ram_used, ram_total, _ = get_memory(device)
            except (RuntimeError, ValueError) as e:
                for non_intuitive_error in constants.NON_INTUITIVE_RAM_ERRORS:
                    if non_intuitive_error in e.args[0]:
                        model_loader.wipe_memory()
                        return False, None, None
                return False, None, e.args[0]

    model_loader.wipe_memory()
    return True, ram_used, None


def try_inference_loop(model_loader,
                       batch_size,
                       input_size,
                       input_depth=None,
                       run_subprocess=True
                       ):
    logging.info(f'Testing inference run with:')
    logging.info(f'batch size: {batch_size}')
    logging.info(f'input size: {input_size}')
    if input_depth is not None:
        logging.info(f'input depth: {input_depth}')

    if run_subprocess:
        calculation_possible, ram_used, error_message = multiprocess_function(
            try_inference_loop_threaded,
            [[model_loader], [batch_size], [input_size], [input_depth]]
        )[0]
    else:
        calculation_possible, ram_used, error_message = try_inference_loop_threaded(
            model_loader, batch_size, input_size, input_depth
        )
    if error_message is not None:
        raise RuntimeError(f'Got an error during calculation:\n{error_message}')
    if calculation_possible:
        logging.info('Success.')
    else:
        logging.info('Failed.')
    return calculation_possible, ram_used


def create_3d_iteration_map():
    """
    Combines those two lists to a map. Example:

    Examples:
        reversed_input_sizes: [256, 196, 128, 64]  # real choices located at constants
        reversed_input_depths: [512, 128, 64, 32, 20]  # real choices located at constants
        -->
        out_map = [
            # (in_size, depth),
            (512, 256),  # input size 512 was not given but will be inserted. volume should be as "cube-like" as possible
            (256, 128),
            (196, 128),
            (128, 128),
            (128, 64),
            (64, 64),
            (64, 32),
            (64, 20),
        ]

    """
    reversed_input_sizes = list(reversed(sorted(
        [size_choice[0] for size_choice in constants.IN_SIZE_CHOICES_3D]
    )))
    reversed_input_depths = list(reversed(sorted(
        [depth_choice[0] for depth_choice in constants.DEPTH_CHOICES_3D]
    )))

    max_shape = max((reversed_input_depths[0], reversed_input_sizes[0]))
    min_shape = min((reversed_input_depths[-1], reversed_input_sizes[-1]))

    if max_shape == reversed_input_sizes[0]:
        pass
    else:
        reversed_input_sizes.insert(0, max_shape)

    if min_shape == reversed_input_depths[-1]:
        pass
    else:
        reversed_input_depths.append(min_shape)

    in_size_in_depth_map = []
    for depth_size in reversed_input_depths:
        for i, input_size in enumerate(reversed_input_sizes):
            if input_size < depth_size:
                break
            if in_size_in_depth_map and input_size > in_size_in_depth_map[-1][0]:
                continue
            in_size_in_depth_map.append((input_size, depth_size))

    return in_size_in_depth_map


def calculate_3d_inference_parameters(model_loader, max_in_size, max_in_depth):
    logging.info(
        f'Calculation procedure:'
        f'\n'
        f'1. use maximum possible input size and depth\n'
        f'2. increase batch size until max is reached'
    )

    used, total, unit = get_memory(model_loader.device)
    if total < 4097:
        max_ram_dependent_depth = 64
    elif total < 6145:
        max_ram_dependent_depth = 128
    elif total < 8193:
        max_ram_dependent_depth = 128
    elif total < 10241:
        max_ram_dependent_depth = 196
    elif total < 12289:
        max_ram_dependent_depth = 196
    else:
        max_ram_dependent_depth = 256  # max if VRAM > 12GB

    in_size_in_depth_map = []
    for in_size, in_depth in create_3d_iteration_map():
        if in_depth <= max_ram_dependent_depth:
            in_size_in_depth_map.append((in_size, in_depth))

    input_size = None
    input_depth = None
    ram_used = 0

    for in_size, in_depth in in_size_in_depth_map:
        if in_size > max_in_size or in_depth > max_in_depth:
            continue
        calculation_possible, ram_used = try_inference_loop(model_loader, 1, in_size, in_depth)
        if calculation_possible:
            input_size = in_size
            input_depth = in_depth
            break

    if input_size is None:
        return None, None, None, None

    if model_loader.device == 'cpu' or model_loader.device == torch.device('cpu'):
        batch_sizes = list(sorted(
            [batch_choice[0] for batch_choice in constants.CPU_INFERENCE_BATCH_SIZE_CHOICES_3D]
        ))
    else:
        batch_sizes = list(sorted(
            [batch_choice[0] for batch_choice in constants.INFERENCE_BATCH_SIZE_CHOICES_3D]
        ))

    batch_size = 1

    for current_batch_size in batch_sizes:
        calculation_possible, current_ram_used = try_inference_loop(
            model_loader, current_batch_size, input_size, input_depth
        )
        if calculation_possible:
            batch_size = current_batch_size
            ram_used = current_ram_used
        else:
            break

    return input_size, input_depth, batch_size, ram_used


def log_and_return_stored_values(file_path):
    config = read_csv_to_list(file_path)
    logging.info(f'Found optimal parameters.')
    input_size = int(config[0][0])
    input_depth = int(config[0][1])
    batch_size = int(config[0][2])
    logging.info(f'Calculated parameters: input_size={input_size}, input_depth={input_depth}, batch_size={batch_size}')
    return input_size, input_depth, batch_size


def get_optimal_inference_parameters(model_path, device, max_in_size, max_in_depth=None):
    """
    Run in extra process to release cuda memory completely. Main thread caches due to CUDA drivers
    https://discuss.pytorch.org/t/cuda-memory-is-not-freed-properly/48395/4
    """
    device = torch.device(device)
    checkpoint = torch.load(model_path, map_location=device)
    checkpoint_keys = checkpoint.keys()
    normalization = None

    if 'normalization' in checkpoint_keys:
        normalization = checkpoint['normalization']

    _, total_ram, _ = get_memory(device)

    if checkpoint['architecture'] == 'UNet3D':
        file_path = os.path.join(
            constants.MODEL_DIR, f'UNet3D_{device.type}_input_parameters.txt'
        )
    elif checkpoint['architecture'] == 'DenseVoxelNet':
        file_path = os.path.join(
            constants.MODEL_DIR, f'DenseVoxelNet_{device.type}_input_parameters.txt'
        )
    else:
        raise RuntimeError(f'{checkpoint["architecture"]} is not a supported model.')

    if os.path.exists(file_path):
        return log_and_return_stored_values(file_path)

    logging.info(f'Calculating optimal inference parameters for {checkpoint["architecture"]}')
    logging.info('This might take some time but is done only once on your system.')
    logging.info(f'Configuration will be stored here: {file_path}')
    if device == 'cpu' or device == torch.device('cpu'):
        max_inference_bs_size = max(constants.CPU_INFERENCE_BATCH_SIZE_CHOICES_3D)[0]
        logging.info(
            f'Device is CPU: Batch size limit reduced to {max_inference_bs_size}! Otherwise '
            f'computation will take too long. Check task manager to see ram usage during calculation.'
        )

    model_loader = ModelLoader(
        architecture=checkpoint['architecture'],
        input_channels=checkpoint['image_channels'],
        classes=checkpoint['classes'],
        device=device,
        normalization=normalization,
    )
    if max_in_depth is None:
        raise ValueError('Pass the value for the maximum depth as well!')
    input_size, input_depth, batch_size, ram_used = calculate_3d_inference_parameters(
        model_loader, max_in_size, max_in_depth
    )
    if input_depth is None:
        raise RuntimeError('Calculation failed.')
    if input_size is None or batch_size is None:
        raise RuntimeError('Calculation failed.')
    logging.info(f'Finished. Approximated RAM usage: {ram_used} MiB')

    save_csv(
        file_path,
        [input_size, input_depth, batch_size],
        'Input size, Input depth, Batch size',
        number_format=':.0f',
    )

    logging.info(f'Calculated parameters: input_size={input_size}, input_depth={input_depth}, batch_size={batch_size}')
    return input_size, input_depth, batch_size
