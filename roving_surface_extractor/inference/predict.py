import logging
import os
import time

import torch

from .optimal_parameter_calculation.inference_parameters import get_optimal_inference_parameters
from .predictor.volume_predictor import SlidingVolumePredictor

from .. import constants
from ..file_handling.utils import get_files
from ..dl_models.loading import load_model_from_checkpoint
from ..utils import get_time_spent_string, get_human_datetime


def predict(model_path,
            data_folder,
            batch_size,
            input_size,
            input_depth,
            device,
            decode_to_color,
            overlap,
            binary_logit_threshold=None,
            weight_mode='gauss',
            combine=True,
            logger=None,
            ):
    """

    Args:
        model_path:
        data_folder:
        batch_size:
        input_size:
        input_depth:
        device:
        decode_to_color:
        overlap:
        binary_logit_threshold:
        weight_mode: 'sum', 'linear', 'gauss', see inference.utils for implementation
        combine: whether to combine the predictions and images (only false for testing)
        logger:

    Returns:

    """
    if binary_logit_threshold is None:
        binary_logit_threshold = 0.5

    start_time = time.time()
    logging.info(f'Start time at: {get_human_datetime()}')

    model = load_model_from_checkpoint(model_path, device)
    image_channels = model.in_channels

    predictor = SlidingVolumePredictor(
        folder=data_folder,
        input_size=input_size,
        input_depth=input_depth,
        model=model,
        batch_size=batch_size,
        channels=image_channels,
        decode_to_color=decode_to_color,
        overlap=overlap,
        binary_logit_threshold=binary_logit_threshold,
        weight_mode=weight_mode,
        logger=logger
    )
    if not predictor.already_segmented:
        device = torch.device(device)
        if 'cuda' in device.type:
            if not torch.cuda.is_available():
                logging.info('Cuda is not available on your system. Using cpu.')
                device = torch.device('cpu')

        predictor.predict(device)
        time_string = get_time_spent_string(start_time)
        time_in_seconds = time.time() - start_time
        logging.info(
            f'Finished prediction at: {get_human_datetime()} (time spent: {time_string}; seconds: '
            f'{time_in_seconds:.1f})'
        )
    if combine and not predictor.already_combined:
        predictor.combine_images_and_masks()

    return predictor.out_folder


def start_prediction(params, logger=None):
    model_path = params.model_path
    data_folder = params.data_folder
    batch_size = params.batch_size
    input_size = params.input_size
    input_depth = params.depth_3d
    device = params.device
    overlap = params.overlap
    decode_to_color = params.decode_to_color

    if input_size == 0:
        input_size, input_depth, batch_size = get_optimal_inference_parameters(
            model_path, device, 256, 256
        )

    if not os.path.isfile(model_path):
        raise ValueError('Could not find model path')
    if not os.path.isdir(data_folder):
        raise ValueError('Could not find folder to predict')
    if not get_files(data_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES):
        raise ValueError(
            f"Could not find images in folder. Images must be of type: "
            f"{', '.join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}!")

    return predict(model_path=model_path, data_folder=data_folder, batch_size=batch_size, input_size=input_size,
                   input_depth=input_depth, device=device, decode_to_color=decode_to_color, overlap=overlap,
                   binary_logit_threshold=0.5, weight_mode='gauss', logger=logger)
