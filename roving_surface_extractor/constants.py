import os
from pathlib import Path


def create_folder(folder):
    if not os.path.isdir(folder):
        os.makedirs(folder)


current_path = os.path.dirname(__file__)
PROJECT_DIR = os.path.join(Path(current_path).parent)
create_folder(PROJECT_DIR)


MULTICORE_RAM_RESERVATION = 1.5  # GB
MULTICORE_THREAD_REDUCTION = 1  # Keep 1 thread for main server, 1 for main process of extraction
SUPPORTED_INPUT_IMAGE_TYPES = ['.jpg', '.png', '.bmp', '.tif', '.tiff']
SUPPORTED_MASK_TYPES = ['.png', '.bmp', '.tif', '.tiff']

# Model Names 3D
UNet3D_STRONG_NAME = 'UNet3D (best net)'
UNet3D_ONLINE_NAME = 'UNet3D (if best net fails)'
DENSE_VOX_NET_NAME = 'DenseVoxelNet (if your PC is weak)'

MODEL_CHOICES = (
    (UNet3D_STRONG_NAME, UNet3D_STRONG_NAME),
    (UNet3D_ONLINE_NAME, UNet3D_ONLINE_NAME),
    (DENSE_VOX_NET_NAME, DENSE_VOX_NET_NAME),
)

MODEL_DIR = os.path.join(PROJECT_DIR, 'weights')

MODEL_PATHS = {
    DENSE_VOX_NET_NAME: os.path.join(MODEL_DIR, 'DenseVoxNet_weak_augmented.pt'),
    UNet3D_STRONG_NAME: os.path.join(MODEL_DIR, 'UNet3D_strong_offline_augmented.pt'),
    UNet3D_ONLINE_NAME: os.path.join(MODEL_DIR, 'UNet3D_online_augmented.pt'),
}

LOG_DIR = os.path.join(PROJECT_DIR, 'logs')
create_folder(LOG_DIR)

# fallback for older models which were trained on default mean and std of dataset
DEFAULT_RGB_MEAN = [0.485, 0.456, 0.406]
DEFAULT_RGB_STD = [0.229, 0.224, 0.225]

DEFAULT_GRAY_MEAN = [0.5]
DEFAULT_GRAY_STD = [0.5]

IN_SIZE_CHOICES_3D = [
    (64, 64),
    (128, 128),
    (192, 192),
    (256, 256),
]

DEPTH_CHOICES_3D = [
    (16, 16),
    (32, 32),
    (48, 48),
    (64, 64),
    (96, 96),
    (128, 128),
    (192, 192),
    (256, 256),
]

INFERENCE_BATCH_SIZE_CHOICES_3D = [
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (8, 8),
    (10, 10),
    (16, 16),
    (32, 32),
    (48, 48),
    (64, 64),
]

CPU_INFERENCE_BATCH_SIZE_CHOICES_3D = [
    (2, 2),
    (4, 4),
]

NON_INTUITIVE_RAM_ERRORS = [
    'CUDA out of memory',
    'CUDA error: out of memory',
    'Unable to find a valid cuDNN algorithm to run convolution',
    'cuDNN error: CUDNN_STATUS_NOT_SUPPORTED',
    'DefaultCPUAllocator: not enough memory:',
    'Expected output.numel() <= std::numeric_limits<int32_t>::max() to be true, but got false',
]

MAX_NUM_WORKERS = 10
AMP_ENABLED = False  # enable when using a gpu > RTX 30XX can speed up!
# RTX 2070 super is slower in inference using AMP!
