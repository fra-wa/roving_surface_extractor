import cv2
import logging
import numpy as np
import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(os.path.dirname(current_dir))
sys.path.extend([module_dir])

from roving_surface_extractor import constants
from roving_surface_extractor.logger.logging_manager import LoggingManager
from roving_surface_extractor.utils import get_human_datetime
from roving_surface_extractor.inference.predict import start_prediction
from roving_surface_extractor.user_interaction.parameters import Parameters
from roving_surface_extractor.file_handling.utils import create_folder, copy_or_move_file_or_files, \
    get_files, get_sub_folders
from roving_surface_extractor.file_handling.cluster_saving import save_volume_slices


def create_segmentations(logger=None):
    # These are only initial parameters. You are able to change them by runtime
    parameters = [
        # Name              Value       Help
        ('input_size', 256, 'You can decide the input size of an image passed to the model, keep an eye on the ram!\n'
         'Be careful: it should be smaller or equal than the images to predict!'),
        ('batch_size', 8, 'In case of a 2D or recurrent model: this is the number of images predicted simultaneously.\n'
         'In case of a 3D model, this is the numbers of volumes predicted simultaneously.'),
        ('depth_3d', 20, 'If you use the 3 dimensional Unet, you can set the depth of the model.\n'
         'Some odd values wont work! You might need to test some values.'),
        ('device', 'cuda:1', 'I will try to use cuda if possible, else cpu is forced.'),
        ('overlap', 20, 'Increase this to get smoother edges while recreating the final result'),
        ('decode_to_color', True, 'Whether the segmented images are stored as rgb with colors or grayscale. \n'
         'Gray needs less storage.'),
        ('min_cluster_size', 100 * 100 * 100, 'Minimum voxels a cluster must contain -> reduces artifacts'),
        ('max_threads', 16, 'Max used threads'),
    ]

    parameter_names = [para[0] for para in parameters[:]]
    parameter_values = [para[1] for para in parameters[:]]
    parameter_help = [para[2] for para in parameters[:]]

    parameters = Parameters(parameter_names, parameter_values, parameter_help)

    logging.info('Please set the model path and the folder! Other values are good by default but you can play around.\n'
          'The quality of prediction will not change but you might speed up the process.')
    params = parameters.get_fixed_parameters()

    base_folder_name = r'C:\Users\Franz W\data\roving_no_aug_analysis\test_learning_epoch_'
    total_epochs = 100
    start_epoch = 1
    base_data_folder = r'C:\Users\Franz W\data\roving_no_aug_analysis\test_learning_epoch_data_folder'
    base_checkpoint_name = r'C:\Users\Franz W\data\roving_no_aug_analysis\checkpoint_epoch_'
    checkpoint_ending = '.pt'

    base_files = get_files(base_data_folder)

    for i in range(start_epoch, total_epochs + 1):
        params.data_folder = base_folder_name + str(i).zfill(3)
        create_folder(params.data_folder)
        copy_or_move_file_or_files(base_files, params.data_folder)
        params.model_path = base_checkpoint_name + str(i).zfill(3) + checkpoint_ending

        start_prediction(params, logger=logger)


def create_progress_volume():
    folders = get_sub_folders(r'C:\Users\Franz W\data\roving_no_aug_analysis\learning_progress', depth=1)
    progress_volume = None
    epoch_counter = 0

    for folder in folders:
        if r'progress\progress' in folder or 'data_folder' in folder:
            continue

        segmented_folder = os.path.join(folder, 'segmented')
        img_file = get_files(segmented_folder)[15]

        img = cv2.imread(img_file, -1)

        epoch_number = os.path.basename(folder).split('_')[-1]
        text = f'Epoche {epoch_number}'
        cv2.putText(
            img=img,
            text=text,
            org=(350, 350),
            fontFace=cv2.FONT_HERSHEY_PLAIN,
            fontScale=3,
            color=(168, 173, 69),
            thickness=3,
        )

        images = 15

        if progress_volume is None:
            h, w, c = img.shape
            progress_volume = np.zeros((len(folders) * images, h, w, c), dtype=np.uint8)
            for i in range(images):
                progress_volume[i] = img
        else:
            for i in range(epoch_counter * images, (epoch_counter + 1) * images):
                progress_volume[i] = img
        epoch_counter += 1

    save_volume_slices(progress_volume, r'C:\Users\Franz W\data\roving_no_aug_analysis\progress')


if __name__ == '__main__':
    # current_time = get_human_datetime(datetime_format='%Y%m%d_%H_%M_%S', time_ending='')
    # logger_name = f'log_training_progress_{current_time}'
    # log_dir = os.path.join(constants.project_dir, 'evaluation')
    # create_folder(log_dir)
    # log_file_path = os.path.join(log_dir, logger_name + '.txt')
    #
    # loggerManager = LoggingManager()
    # loggerManager.run_logging_listener_thread()
    # logging_initializer = loggerManager.create_logger(log_handler=log_file_path)
    # logging_initializer.set_up_root_logger()
    #
    # create_segmentations(logger=logging_initializer)
    create_progress_volume()
