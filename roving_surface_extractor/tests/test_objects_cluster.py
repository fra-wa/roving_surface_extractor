import numpy as np
import os

from unittest import TestCase

from roving_surface_extractor.file_handling.general_reading import read_csv_to_array
from roving_surface_extractor.file_handling.utils import create_folder, remove_dir
from roving_surface_extractor.surface_extraction.clusters import Cluster


class ClusterTests(TestCase):
    def setUp(self):
        current_dir = os.path.dirname(__file__)
        test_files_dir = os.path.join(current_dir, 'files')
        self.temp_folder = os.path.join(test_files_dir, 'temp_files')
        create_folder(self.temp_folder)

        point_cloud_dir = os.path.join(test_files_dir, 'point_clouds')
        self.loaded_coordinates = read_csv_to_array(os.path.join(point_cloud_dir, 'cavity_00503.csv'))

        self.points = [
            np.array((0, 0, 0)),
            np.array((0, 1, 0)),
            np.array((0, 2, 0)),
            np.array((1, 0, 0)),
            np.array((1, 1, 0)),
            np.array((1, 2, 0)),
            np.array((2, 0, 0)),
            np.array((2, 1, 0)),
            np.array((2, 2, 0)),
        ]

        self.negative_points = [
            np.array((0, 0, 0)),
            np.array((0, 1, 0)),
            np.array((0, 2, 0)),
            np.array((-1, 0, 0)),
            np.array((-1, 1, 0)),
            np.array((-1, 2, 0)),
            np.array((-2, 0, 0)),
            np.array((-2, 1, 0)),
            np.array((-2, 2, 0)),
        ]

        self.not_origin_points = [
            np.array((20, 10, 15)),
            np.array((20, 11, 15)),
            np.array((20, 12, 15)),
            np.array((21, 10, 15)),
            np.array((21, 11, 15)),
            np.array((21, 12, 15)),
            np.array((22, 10, 15)),
            np.array((22, 11, 15)),
            np.array((22, 12, 15)),
        ]

        self.square_points = [
            np.array((0, 0, 0)),
            np.array((0, 0, 1)),
            np.array((0, 0, 2)),
            np.array((0, 1, 0)),
            np.array((0, 1, 1)),
            np.array((0, 1, 2)),
            np.array((0, 2, 0)),
            np.array((0, 2, 1)),
            np.array((0, 2, 2)),
            np.array((1, 0, 0)),
            np.array((1, 0, 1)),
            np.array((1, 0, 2)),
            np.array((1, 1, 0)),
            np.array((1, 1, 1)),
            np.array((1, 1, 2)),
            np.array((1, 2, 0)),
            np.array((1, 2, 1)),
            np.array((1, 2, 2)),
            np.array((2, 0, 0)),
            np.array((2, 0, 1)),
            np.array((2, 0, 2)),
            np.array((2, 1, 0)),
            np.array((2, 1, 1)),
            np.array((2, 1, 2)),
            np.array((2, 2, 0)),
            np.array((2, 2, 1)),
            np.array((2, 2, 2)),
        ]

    def tearDown(self) -> None:
        if os.path.isdir(self.temp_folder):
            remove_dir(self.temp_folder)

    def test_width_height_depth(self):
        cluster = Cluster(self.points)
        self.assertEqual(cluster.width, 1)
        self.assertEqual(cluster.height, 3)
        self.assertEqual(cluster.depth, 3)

        cluster = Cluster(self.negative_points)
        self.assertEqual(cluster.width, 1)
        self.assertEqual(cluster.height, 3)
        self.assertEqual(cluster.depth, 3)

        cluster = Cluster(self.not_origin_points)
        self.assertEqual(cluster.width, 1)
        self.assertEqual(cluster.height, 3)
        self.assertEqual(cluster.depth, 3)

    def test_move_to_origin(self):
        moved_to_origin_coordinates = [
            [2, 0, 0],
            [2, 1, 0],
            [2, 2, 0],
            [1, 0, 0],
            [1, 1, 0],
            [1, 2, 0],
            [0, 0, 0],
            [0, 1, 0],
            [0, 2, 0],
        ]
        cluster = Cluster(self.negative_points)
        cluster.move_to_origin()
        self.assertListEqual([list(coordinate) for coordinate in cluster.points], moved_to_origin_coordinates)

        moved_to_origin_coordinates = [
            [0, 0, 0],
            [0, 1, 0],
            [0, 2, 0],
            [1, 0, 0],
            [1, 1, 0],
            [1, 2, 0],
            [2, 0, 0],
            [2, 1, 0],
            [2, 2, 0],
        ]
        cluster = Cluster(self.not_origin_points)
        cluster.move_to_origin()
        self.assertListEqual([list(coordinate) for coordinate in cluster.points], moved_to_origin_coordinates)

    def test_min_max_border(self):
        cluster = Cluster(self.points)
        expected_min_border = [0, 0, 0]
        expected_max_border = [2, 2, 0]
        self.assertListEqual(list(cluster.minimum_border_point), expected_min_border)
        self.assertListEqual(list(cluster.maximum_border_point), expected_max_border)

        cluster = Cluster(self.negative_points)
        expected_min_border = [-2, 0, 0]
        expected_max_border = [0, 2, 0]
        self.assertListEqual(list(cluster.minimum_border_point), expected_min_border)
        self.assertListEqual(list(cluster.maximum_border_point), expected_max_border)

        cluster = Cluster(self.not_origin_points)
        expected_min_border = [20, 10, 15]
        expected_max_border = [22, 12, 15]
        self.assertListEqual(list(cluster.minimum_border_point), expected_min_border)
        self.assertListEqual(list(cluster.maximum_border_point), expected_max_border)

    def test_get_binary_volume(self):
        cluster = Cluster(self.square_points)

        vol = cluster.get_binary_volume()
        expected = np.full((3, 3, 3), 255)

        true_false = vol == expected

        for z in range(true_false.shape[0]):
            for y in range(true_false.shape[1]):
                for x in range(true_false.shape[2]):
                    self.assertTrue(true_false[z, y, x])

        cluster = Cluster(self.not_origin_points)

        vol = cluster.get_binary_volume()
        expected = np.zeros((23, 13, 16))
        for coord in self.not_origin_points:
            expected[coord[0], coord[1], coord[2]] = 255

        true_false = vol == expected

        for z in range(true_false.shape[0]):
            for y in range(true_false.shape[1]):
                for x in range(true_false.shape[2]):
                    self.assertTrue(true_false[z, y, x])

    def test_get_surface(self):
        cluster = Cluster(self.square_points)
        cluster.generate_surface(use_padding=True)
        cluster.save(target_folder=self.temp_folder, save_normals=True)

        np.array_equal(np.array(self.square_points), cluster.points)

        self.assertIsNotNone(cluster.binary_volume)
        self.assertIsNotNone(cluster.vertices)
        self.assertIsNotNone(cluster.normals)
        self.assertIsNotNone(cluster.faces)

        self.assertEqual(cluster.binary_volume.shape[0], 3)
        self.assertEqual(cluster.binary_volume.shape[1], 3)
        self.assertEqual(cluster.binary_volume.shape[2], 3)

        self.assertTrue(os.path.isdir(os.path.join(self.temp_folder, 'binary_volume')))
        self.assertTrue(os.path.isfile(os.path.join(self.temp_folder, 'surface.ply')))
        self.assertTrue(os.path.isfile(os.path.join(self.temp_folder, 'coordinates.csv')))

        self.tearDown()
        self.setUp()

        cluster = Cluster(self.loaded_coordinates)
        cluster.generate_surface()
        cluster.save(target_folder=self.temp_folder)

        self.assertIsNotNone(cluster.binary_volume)
        self.assertIsNotNone(cluster.vertices)
        self.assertIsNotNone(cluster.normals)
        self.assertIsNotNone(cluster.faces)

        self.assertTrue(os.path.isdir(os.path.join(self.temp_folder, 'binary_volume')))
        self.assertTrue(os.path.isfile(os.path.join(self.temp_folder, 'surface.ply')))
        self.assertTrue(os.path.isfile(os.path.join(self.temp_folder, 'coordinates.csv')))

    def test_cluster_consistency(self):
        gray_volume = np.zeros((6, 5, 4), dtype=np.uint8)
        gray_volume[:, :, :2] = 10
        gray_volume[:, :, 2:] = 50

        indexes = np.where(gray_volume == 50)  # [[depths], [heights], [widths]]
        coordinates_expected = np.hstack((
            np.expand_dims(indexes[0], 1),
            np.expand_dims(indexes[1], 1),
            np.expand_dims(indexes[2], 1),
        ))
        coordinates = np.vstack((indexes[0], indexes[1], indexes[2])).astype(np.uint16)
        coordinates = np.moveaxis(coordinates, 0, 1)

        self.assertTrue(np.array_equal(coordinates_expected, coordinates))

        cluster = Cluster(coordinates)
        cluster.generate_surface()
        cluster.save(target_folder=self.temp_folder, save_normals=True)

        expected_vol = np.zeros_like(gray_volume)
        expected_vol[:, :, 2:] = 255
        binary_vol = cluster.binary_volume

        self.assertTrue(np.array_equal(binary_vol, expected_vol))

        cluster.generate_subvoxel_accuracy(gray_volume, remove_boundary=False)

        self.tearDown()
        self.setUp()

        gray_volume = np.zeros((6, 5, 4), dtype=np.uint8)
        gray_volume[:, 1:4, :2] = 10
        gray_volume[:, 1:4, 2:] = 50

        indexes = np.where(gray_volume == 50)
        coordinates_expected = np.hstack((
            np.expand_dims(indexes[0], 1),
            np.expand_dims(indexes[1], 1),
            np.expand_dims(indexes[2], 1),
        ))
        coordinates = np.vstack((indexes[0], indexes[1], indexes[2])).astype(np.uint16)
        coordinates = np.moveaxis(coordinates, 0, 1)

        self.assertTrue(np.array_equal(coordinates_expected, coordinates))

        cluster = Cluster(coordinates)
        cluster.generate_surface()
        cluster.save(target_folder=self.temp_folder, save_normals=True)

        expected_vol = np.zeros((gray_volume.shape[0], gray_volume.shape[1] - 1, gray_volume.shape[2]))
        expected_vol[:, 1:4, 2:] = 255
        binary_vol = cluster.binary_volume

        self.assertTrue(np.array_equal(binary_vol, expected_vol))

        cluster.generate_subvoxel_accuracy(gray_volume, remove_boundary=False)

    def test_cluster_get_subvoxel_accuracy(self):
        gray_volume = np.zeros((6, 5, 4), dtype=np.uint8)
        gray_volume[:, 1:4, :2] = 10
        gray_volume[:, 1:4, 2:] = 50

        indexes = np.where(gray_volume == 50)
        coordinates = np.vstack((indexes[0], indexes[1], indexes[2])).astype(np.uint16)
        coordinates = np.moveaxis(coordinates, 0, 1)

        cluster = Cluster(coordinates)

        profiles, filtered_profiles, grid_points = cluster.generate_subvoxel_accuracy(
            gray_volume,
            remove_boundary=False,
            order=1
        )
        profiles[profiles < 0] = 0
        profiles[profiles > 255] = 255
        profiles = profiles.astype(np.uint8)

        a = 0
