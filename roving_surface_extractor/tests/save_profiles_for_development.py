import math

import cv2
import numpy as np
import os
import random

from matplotlib import pyplot as plt

from roving_surface_extractor.file_handling.cluster_reading import load_gray_volume
from roving_surface_extractor.file_handling.cluster_saving import save_volume_slices
from roving_surface_extractor.file_handling.utils import create_folder
from roving_surface_extractor.surface_extraction.clusters import Cluster
from roving_surface_extractor.utils import resize_cv2_or_pil_image

from roving_surface_extractor.logger.logging_manager import LoggingManager

PATH_TO_CLUSTER = r'C:\Users\Franz Wagner\Desktop\unet3d_rovings\unseen_testing_for_subvoxel\segmented\extracted_clusters\cluster_1'

SURFACE_NAME = 'surface.ply'
COORD_NAME = 'coordinates.csv'

VOLUME_FOLDER = r'C:\Users\Franz Wagner\Desktop\unet3d_rovings\unseen_testing_for_subvoxel'

OUT_FOLDER = r'C:\Users\Franz Wagner\Desktop\unet3d_rovings\unseen_testing_for_subvoxel\profiles'

FILTER_GRAPH_FOLDER = os.path.join(OUT_FOLDER, 'filtered_profile_graph')
PROFILE_GRAPH_FOLDER = os.path.join(OUT_FOLDER, 'profile_graph')
PROFILE_VOLUME_FOLDER = os.path.join(OUT_FOLDER, 'positions_in_volume')

create_folder(FILTER_GRAPH_FOLDER)
create_folder(PROFILE_GRAPH_FOLDER)
create_folder(PROFILE_VOLUME_FOLDER)


if __name__ == '__main__':
    loggerManager = LoggingManager()
    loggerManager.run_logging_listener_thread()
    logging_initializer = loggerManager.create_logger()
    logging_initializer.set_up_root_logger()

    cluster = Cluster(coordinates_path=os.path.join(PATH_TO_CLUSTER, COORD_NAME), logger=logging_initializer)

    accuracy = 0.5
    filter_size = 3

    original_volume = load_gray_volume(VOLUME_FOLDER, logging_initializer)
    d, h, w = original_volume.shape
    out_volume = np.zeros((d + 2, h + 2, w + 2, 3), dtype=np.uint8)
    out_volume[1:-1, 1:-1, 1:-1, 0] = original_volume
    out_volume[1:-1, 1:-1, 1:-1, 1] = original_volume
    out_volume[1:-1, 1:-1, 1:-1, 2] = original_volume

    print('Calculating profiles')
    profiles, filtered_profiles, grid_points = cluster.generate_subvoxel_accuracy(
        original_volume,
        accuracy=accuracy,
        segment_size=12,
        remove_boundary=False,
        gauss_derivative_filter_size=filter_size,
        profile_start_offset=-2,
    )
    x_axis = list(range(grid_points.shape[1]))
    x_axis = [val * accuracy for val in x_axis]

    random_profile_indexes = []
    for i in range(100):
        random_profile_indexes.append(random.randint(0, grid_points.shape[0] - 1))

    img = np.zeros((2, profiles.shape[1]), dtype=np.uint8)
    min_size = 200
    factor = math.ceil(200 / profiles.shape[1] * 7)
    img = resize_cv2_or_pil_image(img, int(factor * profiles.shape[1]))

    print('Exporting results')
    for idx in sorted(random_profile_indexes):

        profile = np.round(profiles[idx])

        if np.max(profile) > 255:
            # possible due to interpolation
            scale = 255 / np.max(profile)
            profile = profile * scale
            profile[profile > 255] = 255

        filtered = filtered_profiles[idx]

        filtered_plot = np.zeros(int(filtered.shape[0] + 2 * filter_size))
        filtered_img = np.zeros(int(filtered.shape[0] + 2 * filter_size))
        filtered_plot[:filter_size] = np.nan
        filtered_plot[-filter_size:] = np.nan
        filtered_plot[filter_size:-filter_size] = filtered

        filtered_img[:filter_size] = np.nan
        filtered_img[-filter_size:] = np.nan
        filtered_img[filter_size:-filter_size] = filtered

        filtered_graph_name = f'filtered_profile_{idx}_graph.png'
        profile_graph_name = f'profile_{idx}_graph.png'

        plt.plot(x_axis, filtered_plot)
        plt.legend([f'Gauss derivative filter (filter size: {filter_size})'])
        plt.xlabel(f'Voxel (step_size = {accuracy})')
        plt.ylabel('values')
        # plt.ylim(0, 1.5)
        plt.savefig(os.path.join(FILTER_GRAPH_FOLDER, filtered_graph_name))
        plt.close()

        plt.plot(x_axis, profile)
        plt.legend(['Gray profile'])
        plt.xlabel(f'Voxel (step_size = {accuracy})')
        plt.ylabel('Gray value')
        plt.ylim(0, 255)
        plt.savefig(os.path.join(PROFILE_GRAPH_FOLDER, profile_graph_name))
        plt.close()

        filtered_img[np.argwhere(np.isnan(filtered_img))] = 0
        offset = -1 * np.min(filtered_img)
        filtered_img = filtered_img + offset  # + 1 to get black == no data

        # scale from 0 to 255
        scale = 255 / np.max(filtered_img)
        filtered_img = filtered_img * scale
        filtered_img = np.round(filtered_img)
        filtered_img[np.argwhere(np.isnan(filtered_plot))] = 0
        filtered_img[filtered_img > 255] = 255
        filtered_img.astype(np.uint8)

        filtered_img = np.expand_dims(filtered_img, 0)
        profile_img = np.expand_dims(profile, 0)
        empty = np.zeros_like(profile_img)
        current_img = np.vstack((
            empty,
            empty,
            profile_img,
            profile_img,
            profile_img,
            empty,
            filtered_img,
            filtered_img,
            filtered_img,
            empty,
        ))

        current_img = resize_cv2_or_pil_image(current_img, int(factor * profiles.shape[1]))

        text = f'Profile number {idx}'
        cv2.putText(
            img=current_img,
            text=text,
            org=(5, 45),
            fontFace=cv2.FONT_HERSHEY_DUPLEX,
            fontScale=1,
            color=[255],
            thickness=1,
        )

        gray = np.full((2, current_img.shape[1]), 175)

        img = np.vstack((img, current_img, gray))

        vertex_point = cluster.vertices[idx].astype(int)
        vertex_point = vertex_point + 1

        current_out_slice = out_volume[vertex_point[0]]
        current_out_slice = cv2.circle(
            current_out_slice, (vertex_point[2], vertex_point[1]), radius=1, color=(0, 0, 255), thickness=-1
        )
        text = f'Pt {idx}'
        cv2.putText(
            img=current_out_slice,
            text=text,
            org=(vertex_point[2], vertex_point[1] - 5),
            fontFace=cv2.FONT_HERSHEY_PLAIN,
            fontScale=1.5,
            color=[0, 0, 255],
            thickness=1,
        )
        out_volume[vertex_point[0]] = current_out_slice

    name = f'profiles_and_filters.png'
    cv2.imwrite(os.path.join(OUT_FOLDER, name), img)

    save_volume_slices(out_volume, PROFILE_VOLUME_FOLDER)

    print('Finished')
