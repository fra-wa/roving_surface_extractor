import numpy as np
import os

from unittest import TestCase

from roving_surface_extractor.surface_extraction.clusters import Cluster
from roving_surface_extractor.surface_extraction.histogram_analysis import calculate_subvoxel_profile, \
    gauss_derivative_filter_1d


class ClusterTests(TestCase):
    def setUp(self):
        current_dir = os.path.dirname(__file__)
        test_files_dir = os.path.join(current_dir, 'files')

    def test_calculate_subvoxel_profile(self):
        gray_volume = np.zeros((2, 2, 2), dtype=np.uint8)
        gray_volume[:, :, 0] = 10
        gray_volume[:, :, 1] = 50

        indexes = np.where(gray_volume == 50)
        coordinates = np.vstack((indexes[0], indexes[1], indexes[2])).astype(np.uint16)
        coordinates = np.moveaxis(coordinates, 0, 1)

        cluster = Cluster(coordinates) # Not using logging for multiprocessing
        cluster.generate_surface()

        segment_size = 5
        step_size = 1
        vertices = np.round(cluster.vertices, 1)
        normals = cluster.normals

        for normal in normals:
            self.assertEqual(np.sum(normal), -1)
            self.assertEqual(normal[2], -1)

        expected_vertices = np.array([
            np.array([0, 0, 0]),
            np.array([0, 1, 0]),
            np.array([1, 0, 0]),
            np.array([1, 1, 0]),
        ])
        for vertex in vertices:
            self.assertTrue((expected_vertices == vertex).all(1).any())
            self.assertEqual(sum((expected_vertices == vertex).all(1)), 1)

        gray_values, grid_points = calculate_subvoxel_profile(
            gray_volume,
            segment_size,
            step_size,
            vertices,
            normals,
            3,
            'nearest',
            0.0,
        )
        expected_grid_points = np.array([
            [[0, 0, -2], [0, 0, -1], [0, 0, 0], [0, 0, 1], [0, 0, 2]],
            [[0, 1, -2], [0, 1, -1], [0, 1, 0], [0, 1, 1], [0, 1, 2]],
            [[1, 0, -2], [1, 0, -1], [1, 0, 0], [1, 0, 1], [1, 0, 2]],
            [[1, 1, -2], [1, 1, -1], [1, 1, 0], [1, 1, 1], [1, 1, 2]],
        ])
        self.assertTrue(np.array_equal(expected_grid_points, grid_points))
        expected_gray_values = np.array([
            [10, 10, 10, 50, 50],
            [10, 10, 10, 50, 50],
            [10, 10, 10, 50, 50],
            [10, 10, 10, 50, 50],
        ])
        self.assertTrue(np.array_equal(expected_gray_values, gray_values))

        cluster.generate_surface(use_padding=True)
        vertices = np.round(cluster.vertices, 1)
        normals = np.round(cluster.normals, 2)
        gray_values, grid_points = calculate_subvoxel_profile(
            gray_volume,
            11,
            0.5,
            vertices,
            normals,
            3,
            'constant',
            -1.0,
        )
        self.assertEqual(gray_values.shape[0], grid_points.shape[0])
        self.assertEqual(gray_values.shape[0], 16)

    def test_gauss_derivation_filter_1d(self):
        values = np.array([[0, - 1, 0, 0, 0,   1, 2, 3,   3, 3,  3,    2,  1,   0,  0, 0, -1, 0]])
        expected = np.array([np.nan, np.nan, 0, 0.5, 1, 1, 0.5, 0, 0, -0.5, -1, -1, -0.5, 0, np.nan, np.nan])
        result = gauss_derivative_filter_1d(values, filter_size=1, cval_used_for_mapping=-1)[0]
        self.assertEqual(result.shape[0], values.shape[1] - 2)
        result[np.argwhere(np.isnan(result))] = 255
        expected[np.argwhere(np.isnan(expected))] = 255
        self.assertTrue(np.array_equal(expected, result))

        values = np.array([[0, 0, 0, 100, 200, 100, 0, 0, 0]])
        expected = np.array([50, 62.5, 0, -62.5, -50])
        result = gauss_derivative_filter_1d(values, filter_size=2)[0]
        self.assertEqual(result.shape[0],  values.shape[1] - 4)
        self.assertTrue(np.array_equal(expected, result))

        values = np.array([[0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0]])
        expected = np.array([43.75, 43.75, 0, -43.75, -43.75])
        result = gauss_derivative_filter_1d(values, filter_size=3)[0]
        self.assertEqual(result.shape[0], values.shape[1] - 6)
        self.assertTrue(np.array_equal(expected, result))

        values = np.array([
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
        ])
        expected = np.array([43.75, 43.75, 0, -43.75, -43.75])
        result = gauss_derivative_filter_1d(values, filter_size=3)[0]
        self.assertTrue(np.array_equal(expected, result))

        values = np.array([
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
            [0, 0, 0, 0, 100, 200, 100, 0, 0, 0, 0],
        ])
        expected = np.array([255, 255, 255, 255, 255])
        result = gauss_derivative_filter_1d(values, filter_size=3, cval_used_for_mapping=200)[0]
        result[np.argwhere(np.isnan(result))] = 255
        self.assertTrue(np.array_equal(expected, result))
