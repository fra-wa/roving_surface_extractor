from django.db import models


class LogFile(models.Model):
    log_file_path = models.CharField(
        verbose_name='Log filepath',
        max_length=500,
    )
    success = models.BooleanField(
        verbose_name='Process finished successfully',
        default=False,
    )
    process_id = models.PositiveIntegerField(
        verbose_name='Process ID',
    )
    process_start_datetime = models.DateTimeField(
        verbose_name='Process Start Time',
    )
