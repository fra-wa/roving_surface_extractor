import logging
import os
import traceback

import psutil
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(os.path.dirname(current_dir))
sys.path.extend([module_dir])

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'extractor_site.settings')

import django
django.setup()

from roving_surface_extractor import constants
from roving_surface_extractor.file_handling.utils import get_sub_folders, get_files, create_folder, move_dir
from roving_surface_extractor.inference.predict import start_prediction
from roving_surface_extractor.logger.logging_manager import LoggingManager
from roving_surface_extractor.surface_extraction.cluster_generation import start_extraction
from roving_surface_extractor.user_interaction.parameters import FixedParameters


def main():
    data_dir = r'shell_homogenization_rovings_2023/roving_samples'
    models = [
        r'shell_homogenization_rovings_2023/models/strong_offline_aug/rovings_augmented_UNet3D_best_loss_and_acc_epoch_068.pt'
    ]

    device = 'cuda:0'

    logger_name = f'log_01_multi_roving_extraction_cuda_{device}'
    log_file_path = os.path.join(constants.LOG_DIR, logger_name + '.txt')

    print(f'Setup logger')
    logger_manager = LoggingManager()
    logger_manager.run_logging_listener_thread()

    process_logger = logger_manager.create_logger(log_name=logger_name, log_handler=log_file_path)
    process_logger.set_up_root_logger()
    print(f'Done')
    try:
        folders_and_device = [
            (r'20230525_SITGrid044_KK_T10PA8_vxs_9_5_s150-to-s2100', 'cuda:0'),
            (r'20211209_Q43_Grid_ringA7_vsx_9_4', 'cuda:1'),
            (r'20211201_Q43_Anticrack_ringA7_vxs_9_4', 'cuda:0'),
            (r'20220208_MK_Probe2_nah_ringA7_vxs_8_9', 'cuda:1'),
        ]

        for i, (folder, data_device) in enumerate(folders_and_device):
            if data_device != device:
                continue
            folder = os.path.join(data_dir, folder)
            print(f'Folder: {folder}')
            predicted_dirs = get_sub_folders(folder, depth=1)
            predicted_basenames = [os.path.basename(f) for f in predicted_dirs]
            for model in models:
                aug_strategy = os.path.basename(os.path.dirname(model))
                if aug_strategy in predicted_basenames:
                    continue

                print(f'Model: {model}')

                params = FixedParameters()
                params.data_folder = folder
                params.input_size = 256
                params.depth_3d = 128
                params.overlap = 50
                params.batch_size = 1
                params.device = data_device
                params.decode_to_color = False
                params.model_path = model
                params.max_labels_to_check = 3
                params.skip_postprocessing = False
                params.save_normals = False

                print(f'Starting prediction.')
                start_prediction(params, logger=process_logger)
                segmented_folder = os.path.join(folder, 'segmented')
                combined_folder = os.path.join(folder, 'combined')

                print(f'Extracting roving.')
                start_extraction(
                    segmented_folder,
                    params.max_labels_to_check,
                    params.save_normals,
                    logger=process_logger,
                )
                print(f'Moving results.')
                result_dir = os.path.join(folder, aug_strategy)
                create_folder(result_dir)
                move_dir(segmented_folder, result_dir)
                move_dir(combined_folder, result_dir)
                print(f'Done.')
    except Exception:
        logging.error(traceback.format_exc())
        current_process = psutil.Process(os.getpid())
        children = current_process.children(recursive=True)
        child_running = False
        for child in children:
            if child.status() == 'running':
                child_running = True
                break
        if child_running:
            logging.info(f'Terminating background processes. This may take a few seconds.')
        for child in reversed(children):  # reverse to go backwards through children
            child.terminate()
    finally:
        process_logger.remove_logger_handlers()
        logger_manager.stop_logging()


if __name__ == '__main__':
    main()
