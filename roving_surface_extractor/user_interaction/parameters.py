"""
Usage example at bottom! You can execute this file to see, what happens.

Generic classes for a user interaction guide (console).

Usage:
create a list of parameter names
create a list of default parameter values, the user can or has to update

optional:
    create a list of help strings, so the user knows what to input

Pass those lists to the Parameters class.

EXAMPLE at bottom! Follow main!
"""
try:
    from pytorch_segmentation import constants
    from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder, get_sub_folders
    from pytorch_segmentation.user_interaction.utils import confirm_input_by_type

except ImportError:
    import os
    import sys

    current_dir = os.path.dirname(os.path.abspath(__file__))
    module_dir = os.path.dirname(current_dir)
    sys.path.extend([module_dir])

    from roving_surface_extractor import constants
    from ..file_handling.utils import get_files, get_sub_folders
    from ..user_interaction.utils import confirm_input_by_type


class FixedParameters:
    """
    Class to store fixed parameters after user confirmation for easier usage
    """
    batch_size = None
    data_folder = None
    decode_to_color = None
    depth_3d = None
    device = None
    input_size = None
    model_path = None
    overlap = None
    max_labels_to_check = None
    skip_postprocessing = False
    save_normals = False

    def __init__(self):
        pass

    def get_attribute_name_list(self):
        return sorted([attr for attr in dir(self) if not attr.startswith('__') and not callable(getattr(self, attr))])


class Parameter:
    def __init__(self, name, value, help_string=''):
        """

        Args:
            name: name of this parameter
            value: value of this parameter
            help_string: help for this parameter
        """
        if not isinstance(name, str):
            raise ValueError('name must be a string')
        if not isinstance(help_string, str):
            raise ValueError('question must be a string')

        self.name = name
        self.value = value
        self.help_string = help_string
        self.value_type = type(value)


class Parameters:

    # prediction parameters set by user
    batch_size = None
    data_folder = None
    depth_3d = None
    device = None
    input_size = None
    model_path = None
    overlap = None

    def __init__(self, attribute_name_list, attribute_value_list, attribute_help_list=None):
        """

        Args:
            attribute_name_list (list): list of attribute names, which will be set to this class
            attribute_value_list (list): list of initial default attribute values
            attribute_help_list (list): list containing help for each attribute

        Usage:
            automated interaction guide: call parameters.start_interaction_guide()
                This will return a FixedParameters instance where you can call:
                    fixed_parameters.attribute_name
                to get the value.

            Attention! If passing a path, you might want to extend it to your need since a path is just a string.
            Maybe use .utils.confirm_path_input() in that case.

            you can also only:
                - show_parameters()
                - let the user_update_attribute by index or name of the attribute
                - start_interaction_guide
                - call len(parameters)
                - get_attribute_name_list()
                - iterate like: parameters[i]
        """
        if not isinstance(attribute_name_list, list) or not isinstance(attribute_value_list, list):
            raise ValueError(f'Pass lists the Parameters class!\n'
                             f'Names are of type: {type(attribute_name_list)}\n'
                             f'Values are ot type: {type(attribute_value_list)}')

        if attribute_help_list is None:
            attribute_help_list = len(attribute_name_list) * ['']
        elif not isinstance(attribute_help_list, list):
            raise ValueError(f'Pass lists the Parameters class not {type(attribute_help_list)}')

        if not (len(attribute_name_list) == len(attribute_value_list) == len(attribute_help_list)):
            raise ValueError('attribute names, values and helps must be of same length')

        for idx, name in enumerate(attribute_name_list):
            value = attribute_value_list[idx]
            help_string = attribute_help_list[idx]
            self.__setattr__(name, Parameter(name, value, help_string=help_string))

    def __getitem__(self, i):
        """
        You can iterate over this class

        Args:
            i: index

        Returns: object of class: Parameter
        """
        return getattr(self, self.get_attribute_name_list()[i])

    def __len__(self):
        return len(self.get_attribute_name_list())

    def get_attribute_name_list(self, return_all_attributes=False):
        """
        Args:
            return_all_attributes: if True, all attributes are returned, even if they are not instance of Parameter
        """
        all_attributes = [attr for attr in dir(self) if not attr.startswith('__') and not callable(getattr(self, attr))]
        attributes = []
        for name in all_attributes:
            if isinstance(self.__getattribute__(name), Parameter) or return_all_attributes:
                attributes.append(name)
        return sorted(attributes)

    def user_update_attribute(self, attribute_name_or_index):
        """
        Leads the user through an attribute updating

        Args:
            attribute_name_or_index: index of the attribute at get_attribute_name_list() or the name of the attr
        """
        error_message = None
        try:
            attribute_name = self[attribute_name_or_index].name
        except TypeError:
            attribute_name = attribute_name_or_index
        except IndexError:
            error_message = f'Parameter number {attribute_name_or_index} does not exist!'
            return error_message

        if not isinstance(attribute_name, str):
            raise ValueError('Pass an attribute name or the index of the parameter')

        try:
            attribute = self.__getattribute__(attribute_name)

            if not isinstance(attribute, Parameter):
                raise RuntimeError(f'The attribute {attribute_name} must be a Parameter instance!')

            new_value = input(f'Pass a value for {attribute.name}\n')

            new_value = confirm_input_by_type(new_value, attribute.value_type)
            attribute.value = new_value

        except AttributeError:
            print(f'There is no attribute called {attribute_name}.')

        return error_message

    def show_attributes_and_values(self, ask_help=True):
        if ask_help:
            help_me = input('Show help? (y, n)\n')
            help_me = confirm_input_by_type(help_me, bool)
        else:
            help_me = False

        frame_string = '#####################################################'
        print('')
        print(frame_string)
        print('')

        attribute_list = self.get_attribute_name_list()
        longest_attribute = max([len(attr_name) for attr_name in attribute_list])

        if help_me:
            print('Attributes and help:\n')
            for i in range(len(self)):
                parameter = self[i]

                if not isinstance(parameter, Parameter):
                    continue

                name_string = parameter.name + (longest_attribute - len(parameter.name)) * ' '
                space = (len(str(len(self))) - len(str(i))) * ' '

                if parameter.help_string:
                    help_string_list = parameter.help_string.split('\n')
                    fill_length = len(f'Parameter {i} {space}: {name_string} --> ')
                    fill_string = f'\n{fill_length * " "}'
                    help_string = f'{fill_string}'.join(help_string_list)
                    print(f'Parameter {i} {space}: {name_string} --> {help_string}')
                else:
                    print(f'Parameter {i} {space}: {name_string} --> no help provided.')

            print('\n')

        print('Current attributes and related values:')
        for i in range(len(self)):
            parameter = self[i]
            if not isinstance(parameter, Parameter):
                continue
            name_string = parameter.name + (longest_attribute - len(parameter.name)) * ' '
            space = (len(str(len(self))) - len(str(i))) * ' '
            print(f'Parameter {i} {space}: {name_string} --> {parameter.value}')

        print('')
        print(frame_string)
        print('')

    def get_fixed_parameters(self):
        """
        Returns: clean class with all Parameters where you can call an attribute and directly get it's value.

        In other words:
        parameters.attribute_name.value
        can be replaced with
        this_return_class.attribute_name
        """
        fixed_parameters = FixedParameters()
        for parameter_name in self.get_attribute_name_list(return_all_attributes=True):
            parameter = getattr(self, parameter_name)
            if isinstance(parameter, Parameter):
                fixed_parameters.__setattr__(parameter_name, parameter.value)
            else:
                fixed_parameters.__setattr__(parameter_name, parameter)

        return fixed_parameters

    def start_interaction_guide(self):
        self.show_attributes_and_values()

        input_was_a_parameter = True
        user_changed_something = False  # if user changed something, print values again

        # user can now decide to update a parameter or not
        while input_was_a_parameter:

            parameter_to_update = input('Pass a number to change a parameter or type s to start.\n')

            parameter_to_update = confirm_input_by_type(parameter_to_update, int, exception_key='s')
            if parameter_to_update == 's':
                break

            # calling update sequence
            error_msg = self.user_update_attribute(parameter_to_update)
            if error_msg is not None:
                print(error_msg)
            else:
                user_changed_something = True
                self.show_attributes_and_values(ask_help=False)

        if user_changed_something:
            self.show_attributes_and_values(ask_help=False)

        fixed_parameters = self.get_fixed_parameters()

        return fixed_parameters


def usage_example_instantiating():
    # Fast and ugly parameter lists creation
    # predefine some default values!
    parameters = [
        # Name              Value       Help
        ('device',          'cuda',     'Device to process on (cpu or cuda)'),
        ('epochs',          250,        'Number of total epochs to run'),
        ('input_size',      256,        'Input size in pixels'),
        ('lr',              0.001,      'Initial learning rate, will be adapted by Adam optimizer'),
        ('pretrained',      True,       'Resnet backbone pretrained or not. True speeds up training'),
        ('show_ram',        False,      'Whether to show the used ram (device based) or not'),
        ('use_backbone',    False,      'Whether or not to use a backbone. Must be True for Unet Resnet'),
    ]

    parameter_names = [para[0] for para in parameters[:]]
    parameter_values = [para[1] for para in parameters[:]]
    parameter_help = [para[2] for para in parameters[:]]
    parameters = Parameters(parameter_names, parameter_values, parameter_help)

    return parameters


if __name__ == '__main__':
    some_parameters = usage_example_instantiating()
    fixed_params = some_parameters.start_interaction_guide()
