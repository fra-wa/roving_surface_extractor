import datetime
import os
import psutil
import sys

from django.contrib import messages
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone

from ..models import LogFile


def monitor_view(request, log_pk=None):
    if log_pk is None:
        return redirect('roving_surface_extractor:logs')

    if log_pk == '-1':
        log_file = LogFile.objects.last()

        if log_file is None:
            messages.add_message(request, messages.INFO, 'There is no last log to monitor.')
            return redirect('roving_surface_extractor:parameter_input')
        else:
            local_log_time = timezone.localtime(log_file.process_start_datetime)
            messages.add_message(
                request,
                messages.INFO,
                f"Last log ({local_log_time.strftime('%d.%m.%Y, %H:%M:%S')}). If you want to see a specific log, go to"
                f" old logs."
            )
            return redirect('roving_surface_extractor:monitor', log_pk=log_file.pk)

    log_file = LogFile.objects.get(pk=log_pk)

    p = None
    if psutil.pid_exists(log_file.process_id):
        p = psutil.Process(log_file.process_id)
        start_time = round(p.create_time())
        log_start_time = round(datetime.datetime.timestamp(log_file.process_start_datetime))
        if start_time != log_start_time:
            p = None
        if p is not None and sys.platform == 'linux' and p.status() == 'zombie':
            p = None

    if os.path.isfile(log_file.log_file_path):
        with open(log_file.log_file_path, 'r') as f:
            log_file_data = f.readlines()

    else:
        log_file_data = [
            f'The log file:',
            f'{log_file.log_file_path}',
            f'does not exist.',
            f"It was started on {log_file.process_start_datetime.strftime('%d.%m.%Y, %H:%M:%S')}.",
        ]
    
    log_text = ''.join(log_file_data)

    if request.method == 'POST':
        if p is not None and p.is_running() and 'kill_process' in request.POST.keys():
            p.terminate()
            p = None

    context = {
        'log_text': log_text,
        'process_is_active': False if p is None else True,
        'post_kill_url': reverse('roving_surface_extractor:monitor', kwargs={'log_pk': log_pk}),
        'current_view': 'monitor',
    }

    return render(request,
                  'roving_surface_extractor/monitor.html',
                  context
                  )


