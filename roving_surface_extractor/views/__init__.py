# Bug fix while using multiple processes --> need to set up each subprocess which is loading a model
import django
django.setup()

from .logs import logs_view
from .parameter_input import parameter_input_view
from .monitor import monitor_view
