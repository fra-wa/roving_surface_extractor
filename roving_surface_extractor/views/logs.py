from django.contrib import messages
from django.shortcuts import render

from roving_surface_extractor.models import LogFile


def logs_view(request):

    if request.method == 'POST':
        pass

    if len(LogFile.objects.all()) == 0:
        messages.add_message(request, messages.INFO, 'There are no Logs.')

    context = {
        'logs': LogFile.objects.all().order_by('-process_start_datetime'),
        'current_view': 'logs',
    }
    return render(request, 'roving_surface_extractor/logs.html', context)
