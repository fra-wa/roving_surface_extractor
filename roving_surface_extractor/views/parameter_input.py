import datetime
import logging
import os
import sys
import traceback

import psutil
import time

from django.contrib import messages
from django.shortcuts import redirect
from django.shortcuts import render

from multiprocessing import Process

from django.utils import timezone

from .. import constants
from ..forms.parameter_input import ParameterInputForm
from ..inference.predict import start_prediction
from ..logger.logger import LoggerSingleton
from ..models import LogFile
from ..surface_extraction.cluster_generation import start_extraction
from ..user_interaction.parameters import FixedParameters
from ..utils import get_human_datetime, timezone_datetime, get_memory, log_error_info_and_kill_children
from ..utils import get_time_spent_string


def run_extraction(params, process_logger):
    if not isinstance(params, FixedParameters):
        raise ValueError('params must be a FixedParameters instance!')
    process_logger.set_up_root_logger()
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Start time at: {get_human_datetime()}')
        logging.info(f'Starting prediction')
        out_folder = start_prediction(params, logger=process_logger)

        if not params.skip_postprocessing:
            logging.info(f'Starting roving extraction at: {get_human_datetime()}')
            roving_start_time = time.time()

            start_extraction(
                out_folder,
                params.max_labels_to_check,
                params.save_normals,
                logger=process_logger,
            )
            time_string = get_time_spent_string(roving_start_time)
            logging.info(f'Finished roving extraction at: {get_human_datetime()} (time spent: {time_string})')
        else:
            logging.info(f'Skipping roving extraction')

        time_string = get_time_spent_string(start_time)
        logging.info(f'Finished! Total time spent: {time_string}')
    except Exception as e:
        logging.error(traceback.format_exc())
        log_error_info_and_kill_children()
    finally:
        process_logger.remove_logger_handlers()   # To free resources


def parameter_input_view(request):
    last_log_file = LogFile.objects.last()
    process = None
    if last_log_file is not None:
        if psutil.pid_exists(last_log_file.process_id):
            process = psutil.Process(last_log_file.process_id)
            # start time is already in local timezone
            started_at_string = timezone_datetime(
                datetime.datetime.fromtimestamp(round(process.create_time()))).strftime(
                '%d.%m.%Y, %H:%M:%S'
            )
            # log timezone is server --> utc. convert to local timezone
            log_file_started_time = timezone.localtime(last_log_file.process_start_datetime).strftime(
                '%d.%m.%Y, %H:%M:%S'
            )

            if sys.platform == 'linux' and process.status() == 'zombie':
                process = None
            elif started_at_string == log_file_started_time:
                messages.add_message(
                    request,
                    messages.WARNING,
                    f'A process seems to be running! Only one at a time is allowed since the calculation heavily '
                    f'relies on multiprocessing. Simultaneous runs can cause out of memory errors. Therefore, if you '
                    f'start a new one, the old will be stopped.'
                )
            else:
                process = None

    form = ParameterInputForm()
    if request.method == 'POST':
        form = ParameterInputForm(request.POST)
        if form.is_valid():
            if process is not None and process.is_running():
                process.terminate()

            fcd = form.cleaned_data

            params = FixedParameters()
            params.data_folder = fcd['data_folder']
            params.input_size = fcd['input_size']
            params.depth_3d = fcd['depth_3d']
            params.overlap = fcd['overlap']
            params.batch_size = fcd['batch_size']
            params.device = fcd['device']
            params.decode_to_color = True if fcd['color_space'] == 'rgb' else False
            params.model_path = constants.MODEL_PATHS[fcd['network']]
            params.max_labels_to_check = fcd['max_labels_to_check']
            params.skip_postprocessing = fcd['skip_postprocessing']
            params.save_normals = fcd['save_normals']

            current_time = get_human_datetime(datetime_format='%Y%m%d_%H_%M_%S', time_ending='')
            logger_name = f'log_{current_time}'
            log_file_path = os.path.join(constants.LOG_DIR, logger_name + '.txt')

            logger = LoggerSingleton.get_instance()
            process_logger = logger.create_logger(log_name=logger_name, log_handler=log_file_path)
            p = Process(target=run_extraction, args=(params, process_logger), name=logger_name)
            p.start()

            p = psutil.Process(p.pid)

            last_log_file = LogFile.objects.create(
                log_file_path=log_file_path,
                success=False,
                process_id=p.pid,
                process_start_datetime=datetime.datetime.fromtimestamp(round(p.create_time()))
            )

            return redirect('roving_surface_extractor:monitor', log_pk=last_log_file.pk)

    ram_used, ram_total, ram_unit = get_memory(device=form.fields['device'].initial)

    in_size = form.fields['input_size'].initial
    depth = form.fields['depth_3d'].initial
    batch_size = form.fields['batch_size'].initial

    context = {
        'form': form,
        'current_view': 'parameter_input',
        'process_is_running': True if process is not None else False,
        'device': form.fields['device'].initial,
        'bs_initial': batch_size,
        'in_size_initial': in_size,
        'depth_initial': depth,
        'ram_total': ram_total,
        'ram_available': ram_total - ram_used,
        'ram_unit': ram_unit,
    }

    return render(request, 'roving_surface_extractor/parameter_input.html', context)
