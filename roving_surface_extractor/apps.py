from django.apps import AppConfig


class RovingSurfaceExtractorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'roving_surface_extractor'
    verbose_name = "Roving Surface Extractor"
