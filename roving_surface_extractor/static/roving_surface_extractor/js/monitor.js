var RELOAD_INTERVAL = 10 * 1000;

var csrftoken;
var postKillURL;

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function postKillRequest() {
    document.getElementById("js-refresh-button").disabled = true;
    document.getElementById("js-kill-button").disabled = true;
    let killFormData = new FormData();
    killFormData.append('kill_process', 'true');
    let xhr = new XMLHttpRequest();
    xhr.open('POST', postKillURL, false);
    xhr.setRequestHeader('X-CSRFToken', csrftoken);
    xhr.send(killFormData);
}


window.addEventListener('load', function load(event) {
    csrftoken = getCookie('csrftoken');
    let textArea = document.getElementById("log_data");
    let log_text = document.getElementsByClassName('js-data-holder')[0].dataset.log_text;
    postKillURL = document.getElementsByClassName('js-data-holder')[0].dataset.post_kill_url;

    textArea.value = log_text;
    // auto scroll down in text area
    textArea.scrollTop = textArea.scrollHeight;

    let kill_button = document.getElementById("js-kill-button");
    if (!kill_button.hasAttribute("disabled")) {
        kill_button.addEventListener("click", function () {
            postKillRequest();
        });
        setTimeout(function(){
           window.location.reload(1);
        }, 3000);
    };

});
