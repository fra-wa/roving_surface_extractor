import torch
import warnings

from . import model_constants


def get_group_norm_groups(channels):
    """
    GroupNorm expects:
    num_groups: int
    num_channels: int

    The initial parameters can be extracted out of the original paper:
    https://arxiv.org/pdf/1803.08494.pdf
    The extraction can be found at the model_constants.py

    But in_channels of Conv2D is no constant so this function interpolates the "best fitting" values.

    The Table 3 in paper shows that the num groups has a higher impact on the results as long as channels per group are
    >= 8. This function adapts this behaviour and expects at least 16 channels to split into 2 groups! Otherwise you get
    a single group which equals a layer normalization.

    Args:
        channels: channels of the previous conv layer of the Group norm

    Returns: num_groups


    Examples:
        Output (Groups) for different channels:
        GROUP_NORM_LOOKUP = {
            16: 2,  # -> channels per group: 8
            32: 4,  # -> channels per group: 8
            64: 8,  # -> channels per group: 8
            128: 8,  # -> channels per group: 16
            256: 16,  # -> channels per group: 16
            320: 16,  # -> channels per group: 20
            512: 32,  # -> channels per group: 16
            1024: 32,  # -> channels per group: 32
            2048: 32,  # -> channels per group: 64
            4096: 64,   # -> channels per group: 64
            8192: 128,   # -> channels per group: 64
        }


    """
    if not isinstance(channels, int):
        raise ValueError('channels must be an integer!')

    if channels in model_constants.GROUP_NORM_LOOKUP.keys():
        return model_constants.GROUP_NORM_LOOKUP[channels]

    if channels == 1:
        warnings.warn(
            'Group norm with channels == 1 results to Instance Norm (num_groups=1, num_channels=1). '
            'Please read the paper:\n'
            'https://arxiv.org/pdf/1803.08494.pdf\n'
        )
        return 1

    if channels < 16:
        warnings.warn(
            'Group norm with 2 < channels < 16 results to LayerNorm (num_groups=1). Please read the paper:\n'
            'https://arxiv.org/pdf/1803.08494.pdf\n'
        )
        return 1
    if channels % 2 != 0:
        warnings.warn(
            f'GroupNorm Warning: Can not divide {channels} by at least 2 to split into groups.\n'
            f'Using LayerNorm!'
        )
        return 1

    in_channel_keys = sorted(model_constants.GROUP_NORM_LOOKUP.keys())

    groups = None

    for group_norm_channel in reversed(in_channel_keys):
        if channels >= group_norm_channel:
            groups = model_constants.GROUP_NORM_LOOKUP[group_norm_channel]
            break

    if groups is None:
        raise RuntimeError(f'Error at GroupNorm group calculation: got None as groups.')

    channels_per_group = channels / groups
    while not channels_per_group.is_integer() and not groups <= 1:
        groups = groups // 2
        channels_per_group = channels / groups

    return groups


def group_norm(channels, eps=None, **kwargs):
    if eps is not None:
        return torch.nn.GroupNorm(get_group_norm_groups(channels), channels, eps=eps)
    else:
        return torch.nn.GroupNorm(get_group_norm_groups(channels), channels)
