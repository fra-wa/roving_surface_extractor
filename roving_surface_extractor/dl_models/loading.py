import torch
from .unet3d.unet_3d import UNet3D
from .dense_voxel_net.dense_voxel_net import DenseVoxelNet
import logging

from .. import constants


def get_model(model_name, channels, classes, device, normalization=None, print_information=True):
    """

    Args:
        model_name: name is the same like class.__name__
        channels: input channels of the image
        classes: number of classes to segment into
        device: cuda or cpu
        normalization: 'bn' or 'gn' (batch norm or group norm)
        print_information: True -> shows the parameters for initialization, otherwise nothing will be printed

    Returns: the loaded model on the passed device

    """
    model_class = DenseVoxelNet if model_name == constants.DENSE_VOX_NET_NAME else UNet3D

    if print_information:
        logging.info(
            f'Instantiating model: {model_name} with\n'
            f'      num_classes= {classes}\n'
            f'      normalization= {normalization}\n'
            f'      device= {device}'
        )

    model = model_class(
        image_channels=channels,
        num_classes=classes,
        normalization=normalization,
    )

    return model.to(device)


def load_model_from_checkpoint(model_path, device='cuda'):
    """
    Loads a model ready to use for some segmentation task.

    Args:
        model_path: path to the model
        device: cuda or cpu, you decide

    Returns: model in eval mode

    """
    checkpoint = torch.load(model_path, map_location=device)

    model = get_model(
        model_name=checkpoint['architecture'],
        channels=1,
        classes=2,
        device=device,
        normalization=checkpoint['normalization'],
    )

    try:
        # fallback for old checkpoints --> those were trained on default std and mean values.
        model.dataset_mean = checkpoint['dataset_mean']
        model.dataset_std = checkpoint['dataset_std']
    except KeyError:
        pass

    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()

    return model
