import cv2
import numpy as np
import os
import torch

from ..file_handling.utils import create_folder


class Filter:
    def __init__(self, related_layer, filter_nr, filter_map, global_min, global_max):
        """

        Args:
            related_layer:
            filter_nr: of the related layer, one layer contains n filters
            filter_map: the actual feature map
            global_min: minimum weight of all filters of current group, needed for normalization
            global_max: maximum weight of all filters of current group, needed for normalization
        """
        if not torch.is_tensor(filter_map):
            raise RuntimeError('Filter map is not a torch tensor!')
        self.layer_name = related_layer.replace('.', '_')
        self.out_name = f'filter_nr_{filter_nr}.png'
        self.torch_filter = filter_map
        self.global_min = global_min
        self.global_max = global_max
        if self.global_min < 0:
            self.offset = -1 * self.global_min
        else:
            self.offset = -self.global_min

    def save(self, out_folder):
        # converts filter to range: 0, 255 based on all weights of current con convolution
        np_filter = self.torch_filter.cpu().detach().numpy()

        max_weight = self.global_max + self.offset
        multiplier = 255 / max_weight
        # normalize
        np_filter_image = (np_filter + self.offset) * multiplier
        np_filter_image = np.round(np_filter_image, decimals=0)
        if np.max(np_filter_image) > 255 or np.min(np_filter_image) < 0:
            raise RuntimeError('mistake')
        np_filter_image = np_filter_image.astype(np.uint8)
        np_filter_image = np.moveaxis(np_filter_image, 0, -1)

        out_folder = os.path.join(out_folder, self.layer_name)
        create_folder(out_folder)
        out_path = os.path.join(out_folder, self.out_name)
        cv2.imwrite(out_path, np_filter_image)


class BaseModel(torch.nn.Module):
    def __init__(self, in_channels, num_classes):
        super(BaseModel, self).__init__()
        # mean and standard deviation of the training dataset. This is stored at the checkpoint and is set during
        # runtime while loading from checkpoint.
        self.dataset_mean = None
        self.dataset_std = None
        self.in_channels = in_channels
        if num_classes == 2:
            # binary!
            out_channels = 1
        else:
            out_channels = num_classes
        self.out_channels = out_channels

    @property
    def first_layer_padding(self):
        name, conv = self.get_first_layer_information()
        return conv.padding

    def forward(self, **kwargs):
        raise NotImplementedError('implement at your model')

    def get_parameter_count(self):
        """Counts all trainable parameters of this model
        """
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

    def get_sorted_convolutions_and_names(self, layer=None, layer_name=None):
        """
        iterates through a model or layer and returns all convolutional layers and their names

        Args:
            layer:
            layer_name:

        Returns: list of all convolutions and names of this model like: [(layer_name, convolution)]

        """
        if layer is None:
            layer = self

        convolutions = []
        for name, child in layer.named_children():
            if isinstance(child, torch.nn.Conv2d) or isinstance(child, torch.nn.Conv3d):
                if layer_name is not None:
                    name = f'{layer_name}_{name}'
                convolutions.append((name, child))
            else:
                if layer_name is not None:
                    name = f'{layer_name}_{name}'
                convolutions += (self.get_sorted_convolutions_and_names(child, name))

        return convolutions

    def get_first_layer_information(self):
        name, conv_layer = self.get_sorted_convolutions_and_names()[0]
        return name, conv_layer

    def get_last_layer_information(self):
        name, conv_layer = self.get_sorted_convolutions_and_names()[-1]
        return name, conv_layer
