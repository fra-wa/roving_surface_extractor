"""roving_surface_extractor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from roving_surface_extractor import views

app_name = 'roving_surface_extractor'
urlpatterns = [
    path('', views.parameter_input_view, name='parameter_input'),
    path('logs', views.logs_view, name='logs'),
    path('monitor/<str:log_pk>/', views.monitor_view, name='monitor'),
]
