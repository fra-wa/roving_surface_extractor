import os

import cv2
import torch.cuda

from django import forms

from .. import constants
from ..file_handling.utils import get_files


class ParameterInputForm(forms.Form):

    data_folder = forms.CharField(
        max_length=500,
        label='Folder path of the volume to examine',
        required=True,
        initial='',
        help_text=f'Remove as much air as possible from your data first to speed up the process and reduce '
                  f'segmentation artifacts.',
    )

    network = forms.ChoiceField(
        widget=forms.Select(),
        label='Network',
        choices=constants.MODEL_CHOICES,
        initial=constants.UNet3D_ONLINE_NAME,
        required=True,
        help_text=f'Select the pretrained model. Use {constants.UNet3D_ONLINE_NAME} for best generalized network. '
                  f'Use {constants.DENSE_VOX_NET_NAME} if the UNet is too large for your system. '
                  f'Use {constants.UNet3D_STRONG_NAME} to reproduce results of '
                  f'Surface Extraction of Carbon Textile in CT-Scanned Concrete and its Application in Homogenization '
                  f'for thin Structures',
    )

    input_size = forms.ChoiceField(
        widget=forms.Select(),
        label='Input size',
        choices=[(0, 0)] + constants.IN_SIZE_CHOICES_3D,
        initial=0,
        required=True,
        help_text='0 for automatic calculation. Height and width of the sub volumes. Sub volumes are created out of '
                  'the original volume for prediction and finally combined to create the segmented volume.',
    )

    depth_3d = forms.ChoiceField(
        choices=constants.DEPTH_CHOICES_3D,
        initial=128,
        label='Input depth',
        required=True,
        help_text='Depth of the created sub volumes (see input size) during prediction. '
                  'If Input size is 0, this will be calculated as well. Larger input size and depth give better and '
                  'faster results.',
    )
    overlap = forms.IntegerField(
        min_value=20,
        max_value=80,
        initial=50,
        label='Overlap percentage',
        required=True,
        help_text='Overlap percentage (in width, depth, height) of the generated sub volumes (see input size). '
                  'Increases redundancy, enhances the result. 50% recommended (min: 20%, max: 80%).',
    )
    batch_size = forms.IntegerField(
        min_value=1,
        initial=1,
        label='Batch size',
        required=True,
        help_text='How many sub volumes (see input size) are predicted simultaneously. '
                  'If Input size is 0, this will be calculated as well.',
    )
    device = forms.ChoiceField(
        label='Device',
        choices=[('cpu', 'CPU')],
        required=True,
        help_text='Device on which the network shall compute. If you get memory errors, try to reduce batch size and'
                  ' input sizes.',
    )

    color_space = forms.ChoiceField(
        label='Colorspace',
        choices=[('rgb', 'rgb'), ('gray', 'gray scale')],
        required=True,
        help_text='Color space of the resulting segmentation.',
        initial='rgb',
    )

    skip_postprocessing = forms.BooleanField(
        initial=False,
        required=False,
        help_text='If you only want to segment a volume, you can skip the postprocessing.',
    )
    save_normals = forms.BooleanField(
        initial=False,
        required=False,
        help_text='Whether or not to store surface normals. True approximately doubles the storage size.',
    )

    max_labels_to_check = forms.IntegerField(
        min_value=0,
        initial=1,
        label='Number of Rovings contained',
        required=True,
        help_text='Extract the n largest volumes from the segmentation (roving(s) should be the largest).',
    )

    def __init__(self, *args, **kwargs):
        super(ParameterInputForm, self).__init__(*args, **kwargs)

        if not torch.cuda.is_available():
            self.fields['device'].choices = [('cpu', 'CPU')]
        else:
            device_count = torch.cuda.device_count()
            choices = [('cpu', 'CPU')]
            if device_count == 1:
                choices.append(('cuda:0', 'GPU'))
            else:
                for i in range(device_count):
                    choices.append((f'cuda:{i}', f'GPU:{i}'))
            self.fields['device'].choices = choices
            self.fields['device'].initial = choices[1][0]

    def clean(self):
        fcd = super().clean()
        data_folder = fcd['data_folder']
        device = fcd['device']

        fcd['depth_3d'] = int(fcd['depth_3d'])
        fcd['input_size'] = int(fcd['input_size'])

        depth_3d = fcd['depth_3d']
        input_size = fcd['input_size']

        if not os.path.isdir(data_folder):
            self.add_error('data_folder', f"There is no such directory: {data_folder}")
            files = None
        else:
            files = get_files(data_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)

        if files is not None and not files:
            supported_types_string = ', '.join(constants.SUPPORTED_INPUT_IMAGE_TYPES)
            self.add_error(
                'data_folder',
                f"There are no images to process. Supported types are: {supported_types_string}"
            )
        elif files is not None:
            if len(files) < depth_3d:
                self.add_error(
                    'depth_3d',
                    f'Input depth is larger than the volume depth. Volume has a depth of {files}.'
                )

            h, w = cv2.imread(files[0]).shape[:2]
            if h < input_size or w < input_size:
                self.add_error(
                    'input_size',
                    f'Input size is larger than the volume height or width. Volume has a height of {h} and width of {w}.'
                )

        if 'cuda' in device and not torch.cuda.is_available():
            self.add_error(
                'device',
                'Could not find a suitable graphics card. Ensure, that you have an nvidia gpu and the latest driver'
            )

        return fcd
