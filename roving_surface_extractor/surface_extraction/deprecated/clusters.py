import logging
import math
import numpy as np
import os
import trimesh
import warnings

from skimage import measure

from ...file_handling.cluster_reading import load_gray_volume
from ...file_handling.general_reading import read_csv_to_array
from ...file_handling.utils import create_folder
from ...multicore_utils import get_max_threads
from ...multicore_utils import multiprocess_function
from ...multicore_utils import slice_list_equally
from .histogram_analysis import calculate_subvoxel_profile, gauss_derivative_filter_1d
from ...file_handling.cluster_saving import save_volume_slices, save_d_h_w_coordinates_as_x_y_z, \
    save_d_h_w_coordinates_as_x_y_z_as_binary
from .utils import resize_volume


class Cluster:
    """
    Cluster for regular voxel grid

    Pass an array of coordinates (depth, height, width) at instantiation or a path to coordinates. Then you can:
        - calculate a binary volume
        - calculate the surface (marching cubes) and normals
        - calculate subvoxel accuracy of the surface when given the original volume relating to this cluster

    You can save the following (when called the related functions above):
        - coordinates as csv
        - binary volume as image stack
        - surface as .ply (marching cubes)
        - vector field of normals (used marching cubes)
        - subvoxel accuracy surface points
        - subvoxel accuracy surface as .ply without normals
    """

    def __init__(self,
                 coordinates=None,
                 coordinates_path=None,
                 surface_path=None,
                 binary_volume_folder=None,
                 logger=None,
                 ):
        """

        Args:
            coordinates: list or np array like [np.array([z, y, x]), np.array([..]), ..]
        """
        self.logger = logger
        if coordinates is None and coordinates_path is None:
            raise ValueError('Cant instantiate this cluster! Pass coordinates or at least a path to coordinates.')
        if coordinates is not None and \
                any([coordinates_path is not None, surface_path is not None, binary_volume_folder is not None]):
            raise ValueError(
                'Cluster got coordinates and at least one path to data. Pass either coordinates OR paths to data')
        self.minimum_border_point = None  # imagine a ROI in an image, then this is top left ..
        self.maximum_border_point = None  # .. and this is bottom right
        self.applied_origin_movement = False
        self.origin_movement_offset_d = None  # store offset to cluster - less ram needed during calculations,
        self.origin_movement_offset_h = None  # can be added at the end when exporting
        self.origin_movement_offset_w = None

        # surface mesh
        self.binary_volume = None
        self.applied_padding_for_surface = None
        self.vertices = None
        self.faces = None
        self.normals = None

        # eventually scaled coordinates and binary volume
        self.subvoxel_scaled_coordinates = None
        self.subvoxel_scaled_binary_volume = None
        # surface coordinates in subvoxel accuracy and related values
        self.applied_padding_for_subvoxel_surface = None
        self.subvoxel_coordinates = None
        self.subvoxel_vertices = None
        self.subvoxel_faces = None
        self.subvoxel_normals = None

        self.surface_path = None
        self.vector_path = None

        if coordinates is None:
            self.instantiate_from_files(coordinates_path, surface_path, binary_volume_folder)
        else:
            if isinstance(coordinates, list):
                self.points = np.array(coordinates)
            else:
                self.points = coordinates

        if self.points.dtype != int and self.points.dtype != np.uint8 and self.points.dtype != np.uint16 and \
                self.points.dtype != np.uint32 and self.points.dtype != np.uint64:
            raise ValueError('Data type of the initial coordinates need to be integer!')
        self.update_border_coordinates()

    @property
    def width(self):
        return self.maximum_border_point[2] - self.minimum_border_point[2] + 1

    @property
    def height(self):
        return self.maximum_border_point[1] - self.minimum_border_point[1] + 1

    @property
    def depth(self):
        return self.maximum_border_point[0] - self.minimum_border_point[0] + 1

    def update(self):
        if self.applied_origin_movement:
            self.move_to_origin()
        else:
            self.update_border_coordinates()

    def get_min_max_coordinates(self):

        if self.points.shape[0] == 0:
            return np.array([0, 0, 0]), np.array([0, 0, 0])

        min_x = np.min(self.points[:, 2])
        min_y = np.min(self.points[:, 1])
        min_z = np.min(self.points[:, 0])

        max_x = np.max(self.points[:, 2])
        max_y = np.max(self.points[:, 1])
        max_z = np.max(self.points[:, 0])
        return np.array([min_z, min_y, min_x]), np.array([max_z, max_y, max_x])

    def update_border_coordinates(self):
        """
        Only updates the coordinates! Does not move to origin!
        """
        minimum_coordinates, maximum_coordinates = self.get_min_max_coordinates()
        self.minimum_border_point = minimum_coordinates
        self.maximum_border_point = maximum_coordinates

    def add_points(self, points):
        """
        Args:
            points: list or numpy array like: [np.array([z, y, x]), np.array([..]), ..]
        """

        if isinstance(points, list):
            points = np.array(points)
        elif not isinstance(points, np.ndarray):
            raise ValueError('points must be a list or np.ndarray instance')

        self.points = np.vstack((self.points, points))
        self.update()

    def move_to_origin(self):
        """
        Moves all points to the origin.
        Every point will have coordinates >= 0.
        """
        if self.points.shape[0] == 0:
            self.applied_origin_movement = True
            return

        min_x = np.min(self.points[:, 2])
        min_y = np.min(self.points[:, 1])
        min_z = np.min(self.points[:, 0])

        self.points[:, 2] -= min_x
        self.points[:, 1] -= min_y
        self.points[:, 0] -= min_z

        self.applied_origin_movement = True
        self.origin_movement_offset_d = min_z
        self.origin_movement_offset_h = min_y
        self.origin_movement_offset_w = min_x
        self.update_border_coordinates()

    def invert_origin_movement(self):
        if not self.applied_origin_movement:
            return
        self.points[:, 2] += self.origin_movement_offset_w
        self.points[:, 1] += self.origin_movement_offset_h
        self.points[:, 0] += self.origin_movement_offset_d
        self.applied_origin_movement = False

    def get_binary_volume(self, use_padding=False, store_at_self=True, from_subvoxel_data=False):
        """
        Creates a binary volume with 0 and 255

        Args:
            use_padding: If true, the volume is surrounded by zeroes. Use that to get the surface also at the boundaries
            store_at_self: if True, self.binary_volume will be set --> more ram usage
            from_subvoxel_data: if True, this uses the subvoxel data (if there is any interpolated data!)
        """
        if self.points.shape[0] == 0:
            return None

        if from_subvoxel_data and not self.subvoxel_scaled_coordinates:
            raise ValueError(
                'Cant create a binary volume from upscaled data! Upscaled data is not given, calculate first!'
            )

        if not use_padding and self.binary_volume is not None and not from_subvoxel_data:
            return self.binary_volume
        elif not use_padding and self.subvoxel_scaled_binary_volume is not None and from_subvoxel_data:
            return self.subvoxel_scaled_binary_volume

        z_offset = 0
        y_offset = 0
        x_offset = 0

        if from_subvoxel_data:
            w = np.max(self.subvoxel_scaled_coordinates[:, 2])
            h = np.max(self.subvoxel_scaled_coordinates[:, 1])
            d = np.max(self.subvoxel_scaled_coordinates[:, 0])
            w += 1
            h += 1
            d += 1
        else:
            d, h, w = self.maximum_border_point[:]
            w += 1
            h += 1
            d += 1

        if use_padding:
            x_offset += 1
            y_offset += 1
            z_offset += 1
            volume = np.zeros((d + 2, h + 2, w + 2), dtype=np.uint8)
            if from_subvoxel_data:
                if self.subvoxel_scaled_binary_volume is not None:
                    volume[1:-1, 1:-1, 1:-1] = self.subvoxel_scaled_binary_volume
                else:
                    volume[
                        self.subvoxel_coordinates[:, 0] + z_offset,
                        self.subvoxel_coordinates[:, 1] + y_offset,
                        self.subvoxel_coordinates[:, 2] + x_offset] = 255
            else:
                if self.binary_volume is not None:
                    volume[1:-1, 1:-1, 1:-1] = self.binary_volume
                else:
                    volume[
                        self.points[:, 0] + z_offset,
                        self.points[:, 1] + y_offset,
                        self.points[:, 2] + x_offset] = 255
        else:
            volume = np.zeros((d, h, w), dtype=np.uint8)
            if from_subvoxel_data:
                volume[
                    self.subvoxel_scaled_coordinates[:, 0] + z_offset,
                    self.subvoxel_scaled_coordinates[:, 1] + y_offset,
                    self.subvoxel_scaled_coordinates[:, 2] + x_offset] = 255
            else:
                volume[self.points[:, 0] + z_offset, self.points[:, 1] + y_offset, self.points[:, 2] + x_offset] = 255

        if store_at_self:
            if from_subvoxel_data:
                if use_padding:
                    self.subvoxel_scaled_binary_volume = volume[1:-1, 1:-1, 1:-1]
                else:
                    self.subvoxel_scaled_binary_volume = volume
            else:
                if use_padding:
                    self.binary_volume = volume[1:-1, 1:-1, 1:-1]
                else:
                    self.binary_volume = volume

        return volume

    def scale_cluster(self, scale_factor):
        if scale_factor <= 1:
            raise ValueError('This function is only for subvoxel accuracy. Reduce the cluster before instantiation!')

        if self.binary_volume is not None:
            binary_volume = resize_volume(self.binary_volume, scale=scale_factor, order=2)
        else:
            binary_volume = resize_volume(self.get_binary_volume(), scale=scale_factor, order=2)
        binary_volume[binary_volume < 127.5] = 0
        binary_volume[binary_volume >= 127.5] = 255
        indexes = np.where(binary_volume == 255)
        coordinates = np.vstack((indexes[0], indexes[1], indexes[2])).astype(np.uint16)
        coordinates = np.moveaxis(coordinates, 0, 1)

        self.subvoxel_scaled_coordinates = coordinates
        self.subvoxel_scaled_binary_volume = binary_volume

    def generate_surface(self, store_at_self=True, use_padding=True, from_subvoxel_data=False):
        """
        Generates a surface based on the marching cubes algorithm (by lewiner).
        The mesh lies perfectly over the point cloud.

        1. Generates a binary volume out of the coordinates -> uses a padding of 1 voxel since border voxels cannot be
            reconstructed using the marching cubes!
        2. The binary volume is saved to self without the padding!
        3. The marching cubes is applied and each found vertex coordinate is subtracted by 1!
            Why: due to the padding of the first step.

        This results to eventual negative coordinates for the vertices! But this is intentional

        Attributes:
            store_at_self: if True, result (binary volume and surface attributes) is saved to self --> more ram usage
            use_padding: If False, surfaces at the boundaries are not saved.
            from_subvoxel_data: If self.subvoxel_scaled_coordinates and self.subvoxel_scaled_binary_volume is not None,
                you can calculate the surface using the subvoxel data

        Returns:
                vertices : (V, 3) array
                    Spatial coordinates for V unique mesh vertices. Coordinate order
                    matches input `volume` (M, N, P).
                faces : (F, 3) array
                    Define triangular faces via referencing vertex indices from ``vertices``.
                    This algorithm specifically outputs triangles, so each face has
                    exactly three indices.
                normals : (V, 3) array
                    The normal direction at each vertex, as calculated from the
                    data.
                (values : (V, ) array
                    Gives a measure for the maximum value of the data in the local region
                    near each vertex. This can be used by visualization tools to apply
                    a colormap to the mesh. Not needed here (binary) therefore not returned.)
        """
        if (self.depth < 2 or self.width < 2 or self.height < 2) and not use_padding:
            logging.info(
                f'Using padding to create the clusters surface since marching cubes expects a minimum size\n'
                f'of 2x2x2 but the cluster is of size: {self.depth}x{self.height}x{self.width}')
            use_padding = True

        volume = self.get_binary_volume(
            use_padding=use_padding, store_at_self=store_at_self, from_subvoxel_data=from_subvoxel_data
        )
        unique_values = np.unique(volume)
        if unique_values.shape[0] == 1:
            raise ValueError('The volume contains only coordinates (binary volume only contains values of 255)!\n'
                             'Set use_padding=True if you want a surface!')
        if from_subvoxel_data:
            self.applied_padding_for_subvoxel_surface = use_padding
        else:
            self.applied_padding_for_surface = use_padding

        vertices, faces, normals, values = measure.marching_cubes(volume, 0)

        if use_padding:
            # padding around the whole volume must be removed to fit the Coordinates
            vertices -= 1

        if store_at_self:
            if from_subvoxel_data:
                self.subvoxel_vertices = np.round(vertices, 3)
                self.subvoxel_faces = faces
                self.subvoxel_normals = normals
            else:
                self.vertices = np.round(vertices, 3)
                self.faces = faces
                self.normals = normals

        return vertices, faces, normals

    @staticmethod
    def subvoxel_profile_analyzer(grid_points, gray_values, gradient_calculation_range, gradient_step_size):
        """

        Args:
            grid_points:
            gray_values:
            gradient_calculation_range: how many data points should be used for a gradient calculation
            gradient_step_size: calculate the gradient every x gray values

        Returns:

        """

    def get_subvoxel_profiles(self,
                              volume,
                              segment_size,
                              accuracy,
                              order,
                              extension_mode,
                              cval,
                              vertices,
                              normals,
                              profile_start_offset=0
                              ):
        # d * h * w / 10 ** 9 * 8 cause the volume will internally (scipy) be filtered to float64 -> 8 bit to 64 bit!
        # d * h * w / 10 ** 9 * 4 to ensure calculation plus volume have enough ram
        d, h, w = volume.shape
        max_gb_ram_per_thread = d * h * w / 10 ** 9 * 8 + d * h * w / 10 ** 9 * 4
        max_threads = get_max_threads(logical=True, max_gb_ram_per_thread=max_gb_ram_per_thread)

        total_steps = math.ceil(segment_size / accuracy)

        if vertices.shape[0] * total_steps > 1000 or (vertices.shape[0] > 2 and d * h * w > 250 * 250 * 250):
            vertices_map = slice_list_equally(vertices, max_threads)
            normals_map = slice_list_equally(normals, max_threads)

            original_volume_map = len(vertices_map) * [volume]
            segment_size_map = len(vertices_map) * [segment_size]
            step_size_map = len(vertices_map) * [accuracy]

            order_map = len(vertices_map) * [order]
            extension_mode_map = len(vertices_map) * [extension_mode]
            cval_map = len(vertices_map) * [cval]
            offset_map = len(vertices_map) * [profile_start_offset]

            results = multiprocess_function(
                calculate_subvoxel_profile,
                [
                    original_volume_map,
                    segment_size_map,
                    step_size_map,
                    vertices_map,
                    normals_map,
                    order_map,
                    extension_mode_map,
                    cval_map,
                    offset_map,
                ]
            )
            profiles, grid_points = results[0]
            for idx, (profile, points) in enumerate(results):
                if idx == 0:
                    continue
                profiles = np.vstack((profiles, profile))
                grid_points = np.vstack((grid_points, points))
        else:
            profiles, grid_points = calculate_subvoxel_profile(
                volume,
                segment_size=segment_size,
                step_size=accuracy,
                vertices=self.vertices,
                normals=self.normals,
                order=order,
                extension_mode=extension_mode,
                cval=cval,
                profile_start_offset=profile_start_offset,
            )
        return profiles, grid_points

    @staticmethod
    def filter_profiles(profiles, gauss_derivative_filter_size=3, cval_used_for_mapping=None):
        if not isinstance(profiles, np.ndarray):
            raise ValueError('profiles must be a numpy array')

        if profiles.shape[0] * profiles.shape[1] > 1000 and profiles.shape[0] > 2:
            m, n = profiles.shape
            max_gb_ram_per_thread = m * n / 10 ** 9 * 8 * 3
            max_threads = get_max_threads(logical=True, max_gb_ram_per_thread=max_gb_ram_per_thread)
            profiles_map = slice_list_equally(profiles, max_threads)
            filter_size_map = len(profiles_map) * [gauss_derivative_filter_size]
            cval_map = len(profiles_map) * [cval_used_for_mapping]
            results = multiprocess_function(gauss_derivative_filter_1d, [profiles_map, filter_size_map, cval_map])

            filtered_profiles = results[0]
            for idx, filtered_values in enumerate(results):
                if idx == 0:
                    continue
                filtered_profiles = np.vstack((filtered_profiles, filtered_values))
        else:
            filtered_profiles = gauss_derivative_filter_1d(
                profiles, gauss_derivative_filter_size, cval_used_for_mapping
            )

        return filtered_profiles

    def generate_subvoxel_accuracy(self,
                                   original_volume,
                                   segment_size=10,
                                   accuracy=0.5,
                                   order=3,
                                   extension_mode='constant',  # default is crucial, see documentation
                                   cval=-1.0,  # default is crucial, see documentation
                                   remove_boundary=True,
                                   scale=1,
                                   gauss_derivative_filter_size=3,
                                   profile_start_offset=0,
                                   ):
        """
        1. generates the surface plus normals based on the voxel_data
        2. takes the normals and calculates a line ind 3D at each vertex cutting the surface of the searched cluster in
           the volume
        3. gets all interpolated gray values on the line segment with a given step size
        4. applies a 1d gauss filter [-0.5, 0, 0.5] to the gray profile given by the line
        5. analyzes them with the original data and returns the extracted subvoxel coordinate related to the profile

        Args:
            original_volume: grayscale volume containing the original data
            segment_size (int): in voxel

            accuracy: in voxel; less than 0.5 seems too optimistic
            order: order of the spline interpolation for the gray value interpolation on the line segment
            extension_mode: reflect, grid-mirror, constant, grid-constant, nearest, mirror, grid-wrap, wrap
                More at Notes below.

            cval: fill value for specific modes. can be ignored using the default
            remove_boundary (bool): default True, voxels at the edges of the original_volume will be removed -> no
                subvoxel accuracy possible since information is missing!
            scale: you can up(>1)-scale the original volume to get more points representing the
                surface. This can massively increase the calculation time!
            gauss_derivative_filter_size: for subvoxel accuracy, choose from 1, 2 or 3 (neighbors which are considered)
            profile_start_offset: In voxel. You might want to move the start of the gray normal profile. Currently the
                center of the line segment is at the start grid coordinate (vertices). You can move that using this
                parameter (pass a positive or negative value).
                Example:
                    Initial:
                        start ---------- center ----------- end
                    pass "profile_start_offset = -6" results to:
                        start --- center ------------------ end
                    pass "profile_start_offset = 6" results to:
                        start --------------- center ------ end

        Notes:
            argument segment_size and step_size:
                The created 3D line at self.subvoxel_profile_calculator should start outside the cluster and end
                inside the cluster! Choose the values wisely! See at self.subvoxel_profile_calculator!
                start: vertex + normal * (segment_size/2 * step_size)  -> outside the volume
                end: vertex - normal * (segment_size/2 * step_size)    -> inside the volume

            argument extension_mode:
                The mode parameter determines how the input array is extended beyond its boundaries. Default is constant
                to get explicit -1 if we are outside the volume.
                Behavior for each valid value is as follows (see additional plots and details on boundary modes):

                reflect (d c b a | a b c d | d c b a)
                The input is extended by reflecting about the edge of the last pixel. This mode is also sometimes
                referred to as half-sample symmetric.

                grid-mirror
                This is a synonym for reflect.

                constant (k k k k | a b c d | k k k k)
                The input is extended by filling all values beyond the edge with the same constant value, defined by
                the cval parameter. No interpolation is performed beyond the edges of the input.

                grid-constant (k k k k | a b c d | k k k k)
                The input is extended by filling all values beyond the edge with the same constant value, defined by
                the cval parameter. Interpolation occurs for samples outside the inputs extent as well.

                nearest (a a a a | a b c d | d d d d)
                The input is extended by replicating the last pixel.

                mirror (d c b | a b c d | c b a)
                The input is extended by reflecting about the center of the last pixel. This mode is also sometimes
                referred to as whole-sample symmetric.

                grid-wrap (a b c d | a b c d | a b c d)
                The input is extended by wrapping around to the opposite edge.

                wrap (d b c d | a b c d | b c a b)
                The input is extended by wrapping around to the opposite edge, but in a way such that the last point
                and initial point exactly overlap. In this case it is not well defined which sample will be chosen at
                the point of overlap.

            Returns: subvoxel surface coordinates

        """
        if 1 < accuracy <= 0:
            raise ValueError('Accuracy must be > 0 and < 1')
        if accuracy < 0.2:
            warnings.warn(f'Warning: you are using an accuracy below 0.2. This seems unrealistic since the '
                          f'information will be interpolated.\n Consider a lower accuracy like 0.5 or test against a'
                          f' ground truth.')
        if 1 > scale:
            raise ValueError('You can only upscale the volume! Pass scale >= 1')

        if scale > 1:
            self.scale_cluster(scale)

        use_padding = True  # important! otherwise the resulting coordinates will miss certain points!
        # why? We will not have a vertex in every direction when a coordinate is at the boundary. This is the
        # default when using marching cubes!
        if (self.vertices is None or use_padding != self.applied_padding_for_surface) and scale == 1:
            store_at_self = True if self.vertices is None else False
            vertices, faces, normals = self.generate_surface(store_at_self=store_at_self, use_padding=use_padding)
        elif scale > 1:
            store_at_self = True if self.subvoxel_vertices is None else False
            vertices, faces, normals = self.generate_surface(
                store_at_self=store_at_self, use_padding=use_padding, from_subvoxel_data=True
            )
        else:
            vertices = self.vertices
            normals = self.normals

        profiles, grid_points = self.get_subvoxel_profiles(
            original_volume,
            segment_size,
            accuracy,
            order,
            extension_mode,
            cval,
            vertices,
            normals,
            profile_start_offset,
        )

        if extension_mode not in ['constant', 'grid-constant']:
            cval = None
        filtered_profiles = self.filter_profiles(profiles, gauss_derivative_filter_size, cval)

        if remove_boundary:
            points_to_keep_d = 0 > grid_points[:, 0] < original_volume.shape[0] - 1
            points_to_keep_h = 0 > grid_points[:, 1] < original_volume.shape[1] - 1
            points_to_keep_w = 0 > grid_points[:, 2] < original_volume.shape[2] - 1
            points_to_keep = points_to_keep_d + points_to_keep_h + points_to_keep_w

            grid_points = grid_points[points_to_keep]
            profiles = profiles[points_to_keep]

        return profiles, filtered_profiles, grid_points

    @staticmethod
    def save_ascii_vector_field(vertices, normals, title, filename):
        """Code borrowed from Frank Liebold, TUD (frank.liebold@tu-dresden.de)"""
        with open(filename, 'wb') as f:
            # % HEADER
            f.write(b"# vtk DataFile Version 2.0\n")
            f.write(title.encode('ascii'))
            f.write(b"\nASCII\nDATASET UNSTRUCTURED_GRID\n\n")
            f.write(b"POINTS %d float\n" % vertices.shape[0])
            for k in range(vertices.shape[0]):
                f.write(b"%.2f %.2f %.2f\n" % (vertices[k, 0], vertices[k, 1], vertices[k, 2]))

            f.write(b"\nPOINT_DATA %d\n" % vertices.shape[0])
            f.write(b"VECTORS vectors float\n")
            for k in range(normals.shape[0]):
                f.write(b"%.2f %.2f %.2f\n" % (normals[k, 0], normals[k, 1], normals[k, 2]))

    @staticmethod
    def save_binary_vector_field(vertices, normals, title, filename):
        """Code borrowed from Frank Liebold, TUD (frank.liebold@tu-dresden.de)"""
        with open(filename, 'wb') as f:
            # % HEADER
            f.write(b"# vtk DataFile Version 2.0\n")
            f.write(title.encode())
            f.write(b"\nBINARY\nDATASET UNSTRUCTURED_GRID\n\n")
            f.write(b"POINTS %d float\n" % vertices.shape[0])
            for k in range(vertices.shape[0]):
                # https://numpy.org/devdocs/user/basics.byteswapping.html
                pp1 = np.array([vertices[k, 0], vertices[k, 1], vertices[k, 2]], dtype=np.float32)
                pp1b = pp1.byteswap().newbyteorder()
                f.write(pp1b.tobytes())

            f.write(b"\nPOINT_DATA %d\n" % vertices.shape[0])
            f.write(b"VECTORS vectors float\n")
            for k in range(normals.shape[0]):
                pp1 = np.array([normals[k, 0], normals[k, 1], normals[k, 2]], dtype=np.float32)
                pp1b = pp1.byteswap().newbyteorder()
                f.write(pp1b.tobytes())

    def save(self, target_folder, save_normals=True, surface_as_binary=True, coordinates_as_binary=True):
        """
        Saves all reasonable attributes (if set) into the target folder.
        Reasonable:
            - binary volume as images
            - point cloud
            - surface mesh and normals

        Args:
            target_folder:
            save_normals: if a surface is stored at self, the normals and a vector field of normals are
                saved as well
            surface_as_binary: if True, the surface.ply and vector_field.vtk are represented as a binary
            coordinates_as_binary: if True, a coordinates.ply will be saved instead of a .csv

        Returns:

        """
        if self.points.shape[0] == 0:
            logging.info('Cluster is empty')
            return
        coordinates_name = 'coordinates.ply' if coordinates_as_binary else 'coordinates.csv'
        point_path = os.path.join(target_folder, coordinates_name)
        if not os.path.isdir(target_folder):
            create_folder(target_folder)

        if coordinates_as_binary:
            save_d_h_w_coordinates_as_x_y_z_as_binary(self.points, point_path)
        else:
            save_d_h_w_coordinates_as_x_y_z(self.points, point_path)

        try:
            if self.binary_volume is not None:
                save_volume_slices(self.binary_volume, os.path.join(target_folder, 'binary_volume'))

            if self.faces is not None and self.subvoxel_faces is None:
                self.surface_path = os.path.join(target_folder, 'surface.ply')
                self.vector_path = os.path.join(target_folder, 'vector_field.vtk')

                # Trimesh expects: x, y, z
                swapped_vertices = np.fliplr(self.vertices)
                swapped_faces = np.fliplr(self.faces)
                swapped_normals = np.fliplr(self.normals)

                if save_normals:
                    mesh = trimesh.Trimesh(
                        vertices=swapped_vertices, faces=swapped_faces, vertex_normals=swapped_normals
                    )
                    if surface_as_binary:
                        self.save_binary_vector_field(
                            swapped_vertices, swapped_normals, 'Vector field', self.vector_path
                        )
                    else:
                        self.save_ascii_vector_field(
                            swapped_vertices, swapped_normals, 'Vector field', self.vector_path
                        )
                else:
                    mesh = trimesh.Trimesh(vertices=swapped_vertices, faces=swapped_faces)

                encoding_kwargs = {'encoding': 'binary'} if surface_as_binary else {'encoding': 'ascii'}
                mesh.export(self.surface_path, **encoding_kwargs)

            elif self.subvoxel_coordinates is not None:
                surface_path = os.path.join(target_folder, 'surface.ply')
        except MemoryError:
            logging.info(f'Not enough RAM to save all objects.')

    def instantiate_from_files(self, coordinates_path, surface_path=None, binary_volume_folder=None):
        if not os.path.isfile(coordinates_path):
            raise FileExistsError(f'The coordinates file ({coordinates_path}) does not exist.')

        if surface_path is not None:
            if not os.path.isfile(surface_path):
                raise FileExistsError(f'The surface file ({surface_path}) does not exist.')

        if binary_volume_folder is not None:
            if not os.path.isdir(binary_volume_folder):
                raise FileExistsError(f'The binary volume folder ({binary_volume_folder}) does not exist.')

        self.points = read_csv_to_array(coordinates_path, delimiter=',', dtype=np.uint16)
        self.points = np.fliplr(self.points)

        if surface_path is not None:
            surface = trimesh.load_mesh(surface_path)

            self.normals = np.fliplr(surface.face_normals)
            self.faces = np.fliplr(surface.faces)
            self.vertices = np.fliplr(surface.vertices)

        if binary_volume_folder is not None:
            self.binary_volume = load_gray_volume(binary_volume_folder, self.logger)
