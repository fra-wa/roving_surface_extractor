import math
import warnings

import cv2
import numpy as np

from scipy.ndimage import map_coordinates


def calculate_gradient_2d(x, y):
    """

    Args:
        x: x axis (list or array)
        y: related y values (list or array)

    Returns: slope

    Calculation:
        Fitting: data to y = mx + n
        returns m

    """
    mn = np.polyfit(x, y, 1)
    return mn[0]


def is_start_point(previous, current_and_next):
    """
    Relates to analyze_gradients_and_indexes

    Returns true if the current gray value is the "end" of an interesting region of the histogram.
    """
    previous_zero = True
    for i in range(len(previous)):
        if round(previous[i]) != 0:
            previous_zero = False

    if not previous_zero:
        return False

    current_and_next_increasing = True
    for i in range(len(current_and_next)):
        # if the previous elements are 0, then the next gradients must be positive
        if round(current_and_next[i]) <= 0:
            current_and_next_increasing = False

    return current_and_next_increasing


def is_end_point(previous, current_and_next):
    """
    Relates to analyze_gradients_and_indexes

    Returns true if the current gray value is the "start" of an interesting region of the histogram.
    """
    current_and_next_zero = True
    for i in range(len(current_and_next)):
        if round(current_and_next[i]) != 0:
            current_and_next_zero = False

    if not current_and_next_zero:
        return False

    previous_decreasing = True
    for i in range(len(previous)):
        # if the next elements are 0, then we can look for the previous. Gradients must be negative.
        if round(previous[i]) >= 0:
            previous_decreasing = False

    return previous_decreasing


def find_start_end_values(gradients_and_indexes, points_of_interest):
    """
    Related to: analyze_gradients_and_indexes()
    See analyze_gradients_and_indexes() for explanation
    """
    gradient_count = len(gradients_and_indexes)

    for i in range(3, gradient_count - 3):
        previous_three = []
        current_and_next_two = []

        for j in range(3):
            previous_three.append(gradients_and_indexes[i - (3 - j)][0])
            current_and_next_two.append(gradients_and_indexes[i + j][0])

        found_start_point = is_start_point(previous_three, current_and_next_two)
        found_end_point = is_end_point(previous_three, current_and_next_two)

        if found_start_point:
            points_of_interest['start_values'].append(gradients_and_indexes[i][1])
        if found_end_point:
            points_of_interest['end_values'].append(gradients_and_indexes[i][1])

    return points_of_interest


def find_turning_points(gradients_and_indexes, points_of_interest):
    """
    Related to: analyze_gradients_and_indexes()
    See analyze_gradients_and_indexes() for explanation
    """
    for i in range(1, len(gradients_and_indexes) - 1):
        found_pos_turning_point = False
        found_neg_turning_point = False
        found_pos_wave_point = False
        found_neg_wave_point = False

        current = gradients_and_indexes[i][0]
        previous = gradients_and_indexes[i - 1][0]
        next_slope = gradients_and_indexes[i + 1][0]

        # found turning point
        if previous > current < next_slope or previous < current > next_slope:
            # check if we found a positive or negative turning point
            if current > 0 and previous > 0 and next_slope > 0:
                found_pos_turning_point = True
                if previous > current < next_slope:
                    found_pos_wave_point = True
            elif current < 0 and previous < 0 and next_slope < 0:
                found_neg_turning_point = True
                if previous < current > next_slope:
                    found_neg_wave_point = True

        if found_pos_turning_point:
            points_of_interest['pos_turning_points'].append(gradients_and_indexes[i][1])
        if found_neg_turning_point:
            points_of_interest['neg_turning_points'].append(gradients_and_indexes[i][1])
        if found_pos_wave_point:
            points_of_interest['pos_wave_points'].append(gradients_and_indexes[i][1])
        if found_neg_wave_point:
            points_of_interest['neg_wave_points'].append(gradients_and_indexes[i][1])

    return points_of_interest


def find_min_or_max(gradients_and_indexes, points_of_interest):
    """
    Related to: analyze_gradients_and_indexes()
    See analyze_gradients_and_indexes() for explanation
    """
    for i in range(1, len(gradients_and_indexes)):

        found_local_min = False
        found_local_max = False

        current = gradients_and_indexes[i][0]
        previous = gradients_and_indexes[i - 1][0]

        # local max or min might not be recognized when using large step sizes. catching them here in this elif
        if previous > 0 > current:
            if abs(previous) <= abs(current):
                points_of_interest['local_maxima'].append(gradients_and_indexes[i - 1][1])
            else:
                points_of_interest['local_maxima'].append(gradients_and_indexes[i][1])
        elif previous < 0 < current:
            if abs(previous) <= abs(current):
                points_of_interest['local_minima'].append(gradients_and_indexes[i - 1][1])
            else:
                points_of_interest['local_minima'].append(gradients_and_indexes[i][1])

        if found_local_max:
            points_of_interest['local_maxima'].append(gradients_and_indexes[i][1])
        if found_local_min:
            points_of_interest['local_minima'].append(gradients_and_indexes[i][1])

    return points_of_interest


def analyze_gradients_and_indexes(gradients_and_indexes):
    """
    Compares previous, current and next gradient to see if we have a local max, min or turning point
    Is also able to find start and end values of interesting intervals (start and end values).

    Examples:
        start and end values:
            - if previous 3 gradients are almost 0 and current as well as next 2 != 0 and are increasing then we have a
             start point
            - if previous 3 gradients are not 0 and constantly decreasing and current as well as next 2 are 0 we have an
             end point
            - Todo: might need to be enhanced

        pos or neg _wave_points:
            - i mean special turning points like the "o" beneath (no real saddle point since gradient is not 0):
            - u is a normal turning point

                                                 x
                                               x
                                        o
                                x
                            x
                          u
                        x
                    x
            x

    Args:
        gradients_and_indexes: list of gradients and related indexes (equals gray values)

    Returns: dict like:
        {
            local_maxima: [idx1, idx2, ..],
            local_minima: [idx1, idx2, ..],
            pos_turning_points: [idx1, idx2, ..]
            neg_turning_points: [idx1, idx2, ..]
            pos_wave_points: [idx1, idx2, ..]
            neg_wave_points: [idx1, idx2, ..]
            start_values: [idx1, idx2, ..]
            end_values: [idx1, idx2, ..]
        }

    """

    points_of_interest = {
        'local_maxima': [],
        'local_minima': [],
        'pos_turning_points': [],
        'neg_turning_points': [],
        'pos_wave_points': [],
        'neg_wave_points': [],
        'start_values': [],
        'end_values': [],
    }

    # Split into separate functions for readability and understanding. One for loop was a brain fuck.
    points_of_interest = find_min_or_max(gradients_and_indexes, points_of_interest)
    points_of_interest = find_start_end_values(gradients_and_indexes, points_of_interest)
    points_of_interest = find_turning_points(gradients_and_indexes, points_of_interest)

    for pos_wave_point in points_of_interest['pos_wave_points']:
        if pos_wave_point in points_of_interest['pos_turning_points']:
            index_to_pop = points_of_interest['pos_turning_points'].parameter_input_view(pos_wave_point)
            points_of_interest['pos_turning_points'].pop(index_to_pop)

    for neg_wave_point in points_of_interest['neg_wave_points']:
        if neg_wave_point in points_of_interest['neg_turning_points']:
            index_to_pop = points_of_interest['neg_turning_points'].parameter_input_view(neg_wave_point)
            points_of_interest['neg_turning_points'].pop(index_to_pop)

    return points_of_interest


def get_gradient_calculation_parts(histogram, gradient_calculation_range, gradient_calculation_step_size):
    """
    Splits the data into parts so that the gradient calculation can iterate
    over lists containing indexes (x parts) and gray values (y parts).

    Examples:
        - gradient_calculation_range = 3
        - gradient_calculation_step_size = 1

            The first x_part would look like: [-1, 0, 1]
            The second: [0, 1, 2]
            Third: [1, 2, 3], ....

        - gradient_calculation_range = 3
        - gradient_calculation_step_size = 2

            The first x_part would look like: [-1, 0, 1]
            The second: [1, 2, 3]
            Third: [3, 4, 5], ....

        - the -1 is inserted to be able to calculate the gradient also at the first and last index
        - the related histogram value is set to 0 automatically

    Args:
        histogram: the image histogram
        gradient_calculation_range: range in which a gradient should be calculated (must be odd and > 2)
        gradient_calculation_step_size: step size to move forward

    Returns: x_axis_parts, y_axis_parts

    """
    if gradient_calculation_range < 3 or gradient_calculation_range % 2 == 0:
        raise ValueError('The gradient calculation range must be an odd number equally or larger than 3!')

    x = list(range(0, 256))
    y = list(histogram)

    range_offset = int(gradient_calculation_range / 2)

    x_insert_start = []
    x_insert_end = []
    # Add values to x and y to not loose information over the first few elements since the
    # gradients need a number of range_offset indexes before first and after last calculation
    # Example:
    #   gradient_calculation_range = 5, range_offset = 2
    #   x will look like: [-2, -1, 0, 1, ...., 254, 255, 256, 257]
    #   y will look like: [ 0,  0, l, m, ....,  n ,  o ,  0 ,  0 ] where l .. o are summed gray values
    for i in range(1, range_offset + 1):
        x_insert_start.append(x[0] - i)
        x_insert_end.append(x[-1] + i)

    for i in range(len(x_insert_start)):
        x.append(x_insert_end[i])
        y.append(0)
        x.insert(0, x_insert_start[i])
        y.insert(0, 0)

    x_parts = []
    y_parts = []

    # split data into sub parts for gradient calculation
    # example: range=3 step_size=7 -> first index is 0, second idx is 3, last idx is 255, the one before: 252
    for i in range(range_offset, len(x) - range_offset, gradient_calculation_step_size):
        x_parts.append(x[i - range_offset:i + range_offset + 1])
        y_parts.append(y[i - range_offset:i + range_offset + 1])

    return x_parts, y_parts


def analyze_histogram(img,
                      gradient_calculation_range=9,
                      gradient_calculation_step_size=3,
                      minimum_histogram_value=100):
    """
    Imagine a function which almost perfectly fits your histogram, then this function returns
    the
        - local minima gray values
        - local maxima gray values
        - positive and negative turning points
        - histogram interval start and end values (approximated)

    Args:
        img: grayscale image
        gradient_calculation_range: how many data points should be used for a gradient calculation
        gradient_calculation_step_size: calculate the gradient every x gray values
        minimum_histogram_value: Every SUMMED VALUE of the histogram below that will be set to 0. This is useful to only
            get large gray value differences and not small fragments which can lead to many minima and maxima.
            Defaults to 100

    Returns: list of thresholds representing interesting points like:
        {
            local_maxima: [idx1, idx2, ..],
            local_minima: [idx1, idx2, ..],
            pos_turning_points: [idx1, idx2, ..]
            neg_turning_points: [idx1, idx2, ..]
            pos_wave_points: [idx1, idx2, ..]
            neg_wave_points: [idx1, idx2, ..]
            start_values: [idx1, idx2, ..]
            end_values: [idx1, idx2, ..]
        }
    """
    if gradient_calculation_range < 3 or gradient_calculation_range % 2 == 0:
        raise ValueError('The gradient calculation range must be an odd number equally or larger than 3!')

    histogram = np.squeeze(cv2.calcHist([img], [0], None, [256], [0, 256]))

    histogram[histogram < minimum_histogram_value] = 0

    x_parts, y_parts = get_gradient_calculation_parts(
        histogram, gradient_calculation_range, gradient_calculation_step_size
    )

    # calculate gradients
    gradients_and_indexes = []
    for idx, (x_part, y_part) in enumerate(zip(x_parts, y_parts)):
        gradient = calculate_gradient_2d(x_part, y_part)
        gray_value = idx * gradient_calculation_step_size
        gradients_and_indexes.append((gradient, gray_value))

    points_of_interest = analyze_gradients_and_indexes(gradients_and_indexes)
    return points_of_interest


def get_minima_thresholds(img,
                          gradient_calculation_range=9,
                          gradient_calculation_step_size=1,
                          minimum_histogram_value=100):
    """
    Imagine a function which almost perfectly fits your histogram, then this function returns
    the local minima gray values to separate the image into.

    Args:
        img: grayscale image
        gradient_calculation_range: how many data points should be used for a gradient calculation
        gradient_calculation_step_size: calculate the gradient every x gray values
        minimum_histogram_value: Every SUMMED VALUE of the histogram below that will be set to 0. This is useful to only
            get large gray value differences and not small fragments which can lead to many minima and maxima.
            Defaults to 100

    Returns: list of thresholds representing local minima

    """

    points_of_interest = analyze_histogram(
        img, gradient_calculation_range, gradient_calculation_step_size, minimum_histogram_value
    )

    return points_of_interest['local_minima']


def calculate_subvoxel_profile(volume,
                               segment_size,
                               step_size,
                               vertices,
                               normals,
                               order,
                               extension_mode,
                               cval,
                               profile_start_offset=0,
                               ):
    """
    Given a volume, normal(s) and the related vertices (start coordinates of the normals), this calculates a profile
    along the normal with end coordinates at [+ segment_size/2, - segment_size / 2].
    
    B-Spline interpolation used by scipy is based on:
    "Fast B-spline transforms for continuous image representation and interpolation."
    IEEE Transactions on pattern analysis and machine intelligence 13.3
    (1991): 277-285.
    poles: See Table 2 in the paper

    Args:
        volume (np.ndarray):
        segment_size (int): odd recommended, in voxel
        step_size: 0 to 1
        vertices (np.ndarray): start coordinates of the normals
        normals (np.ndarray): 
        order (int): 1 to 5, 3 is recommended
            Creating lower degree curves results in faster performance during subsequent operations such as
            machining, display, etc. Using higher degree curves reduces the chance of transferring data to other
            systems which may not support them. Also, the creation of a high degree curve through many points may
            lead to unpredictable results.
        extension_mode (str): how the volume is extended at its borders. See self.generate_subvoxel_accuracy
        cval: fill value at borders for specific extension_modes. See self.generate_subvoxel_accuracy
        profile_start_offset: In Voxel. You might want to move the start of the line segment. Currently the center of
            the profile is at the start grid coordinate. You can move that using this parameter (pass a positive or
            negative value).
            Example:
                Initial (21 Voxels):
                    start ---------- center ---------- end
                pass "profile_start_offset = -6" results to:
                    start ---- center ---------------- end
                pass "profile_start_offset = 6" results to:
                    start ---------------- center ---- end

    Explanation:
        Surface: represented by vertices, faces (triangles) and each vertex has a normal
        Normals: pointing away from the volume (and as well away from the surface representing the volume)

        This function uses the vertex coordinate and the normal as direction to represent a line in 3D.
        The line length is defined as (assuming segment_size is odd):
            start: vertex + normal * (segment_size//2 * step_size)  -> outside the volume
            end: vertex - normal * (segment_size//2 * step_size)    -> inside the volume

        The line can now be seen as x-axis in a 2D coordinate system
        The y-axis represents the gray value at each step of the x-axis
    """
    if not isinstance(segment_size, int):
        raise ValueError('Segment size must be an integer! This represents the length of the segment in voxel.')
    if volume.dtype == np.uint8:
        # convert type to get floating point precision of interpolated gray values
        volume = volume.astype(np.float32)

    total_steps = math.ceil(segment_size / step_size)

    if total_steps % 2 == 0:
        r_values = [math.ceil(total_steps / 2) * step_size]
    else:
        r_values = [total_steps // 2 * step_size]

    if profile_start_offset != 0:
        r_values[0] += profile_start_offset / step_size

    for i in range(total_steps - 1):
        r_values.append(r_values[-1] - step_size)

    if r_values[-1] > 0 or r_values[0] < 0:
        warnings.warn(
            f'Warning: Your offset ({profile_start_offset}) results to line segments which will not cut the vertices.\n'
            f'Formula: coordinate = vertex + r * normal --> r is always {"negative" if r_values[0] < 0 else "positive"}'
        )

    grid_points = np.zeros((vertices.shape[0], len(r_values), 3), dtype=np.float32)

    for idx, r in enumerate(r_values):
        grid_points[:, idx] = vertices + r * normals

    gray_values = np.zeros((vertices.shape[0], len(r_values)))
    for idx in range(gray_values.shape[1]):
        mapping_coordinates = np.moveaxis(grid_points[:, idx], 0, 1)
        mapping_coordinates = np.vstack((mapping_coordinates[0], mapping_coordinates[1], mapping_coordinates[2]))
        values = map_coordinates(
            volume, mapping_coordinates, order=order, mode=extension_mode, cval=cval
        )
        gray_values[:, idx] = values

    return gray_values, grid_points


def gauss_derivative_filter_1d(profiles, filter_size=1, cval_used_for_mapping=None):
    """
    Calculates the derivations at each data point in profiles.
    The size will be reduced though depending on the filter size. First and last filter_size values will have nan!

    Args:
        profiles (np.ndarray): array of shape (m, n) with m = number of profiles, n = number of values per profile
        filter_size (int): 1 to 3
            1: [-0.5, 0, 0.5]
            2: [-0.125, -0.25, 0, 0.25, 0.125]
            3: [-0.03125, -0.125, -0.15625, 0, 0.15625, 0.125, 0.03125]
        cval_used_for_mapping: when using map_coordinates with constant mode, the profile cant be calculated there. This
            function can handle this if you specify a value. The result will have np.nan values at those positions

    Returns: applied filter on each profile (start and end are shortened to apply the filter)

    """
    if not isinstance(profiles, np.ndarray):
        raise ValueError('profiles must be a numpy nd array.')

    if not len(profiles.shape) == 2:
        raise ValueError('Dimension error: profiles must be 2 dimensional: M x N with m = number of profiles,'
                         ' n = number of values per profile')

    if filter_size not in [1, 2, 3]:
        raise ValueError('filter_size must be 1, 2 or 3 for the following derivation filters:')

    if filter_size == 1:
        filter_map = np.array([-0.5, 0, 0.5])
    elif filter_size == 2:
        filter_map = np.array([-0.125, -0.25, 0, 0.25, 0.125])
    else:
        filter_map = np.array([-0.03125, -0.125, -0.15625, 0, 0.15625, 0.125, 0.03125])

    output = np.zeros((profiles.shape[0], profiles.shape[1] - 2 * filter_size), dtype=float)

    for idx in range(filter_size, profiles.shape[1] - filter_size):
        current_values_to_filter = profiles[:, idx - filter_size:idx + filter_size + 1].astype(float)
        if cval_used_for_mapping is not None:
            # if so, calculation would use data, which does not exist -> wrong results
            rows_to_set_to_zero = np.argwhere(current_values_to_filter == cval_used_for_mapping)[:, 0]
            current_values_to_filter[rows_to_set_to_zero, :] = np.nan
        output[:, idx - filter_size] = np.matmul(current_values_to_filter, filter_map.T)

    return output
