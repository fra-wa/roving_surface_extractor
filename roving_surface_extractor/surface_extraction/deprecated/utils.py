import timeit
import numpy as np

from scipy.ndimage import map_coordinates
from scipy.ndimage import zoom


def resize_volume_old(volume, scale, order=3):
    """
    B-Spline interpolation used by scipy is based on:
    "Fast B-spline transforms for continuous image representation and interpolation."
    IEEE Transactions on pattern analysis and machine intelligence 13.3
    (1991): 277-285.
    poles: See Table 2 in the paper

    Args:
        volume (np.ndarray):
        scale:
        order (int): order of spline interpolation; 1 to 5; 3 is recommended
            Creating lower degree curves results in faster performance during subsequent operations such as
            machining, display, etc. Using higher degree curves reduces the chance of transferring data to other
            systems which may not support them. Also, the creation of a high degree curve through many points may
            lead to unpredictable results.

    Interpolation method:
        Example in 1d: suppose an array with 2 values.
            the indexes are then [0, 1]
            suppose scale = 2
            the target indexes are then [0, 1, 2, 3]
            The values are mapped like:
            [0,       1]
            [0, 1, 2, 3]  -> indexes 1 and 2 are interpolated
        interpolation function is B-Spline with a given order (set by user)

    Returns: resized volume

    """
    d_interpolation_step_size = (volume.shape[0] - 1) / ((volume.shape[0] * scale) - 1)
    h_interpolation_step_size = (volume.shape[0] - 1) / ((volume.shape[1] * scale) - 1)
    w_interpolation_step_size = (volume.shape[0] - 1) / ((volume.shape[2] * scale) - 1)

    new_depth = int(volume.shape[0] * scale)
    new_height = int(volume.shape[1] * scale)
    new_width = int(volume.shape[2] * scale)

    new_depth = 1 if new_depth < 1 else new_depth
    new_height = 1 if new_height < 1 else new_height
    new_width = 1 if new_width < 1 else new_width

    coordinates = np.stack(
        np.meshgrid(
            np.arange(new_depth),
            np.arange(new_width),
            np.arange(new_height)
        ), -1).reshape(-1, 3)

    interpolation_coordinates = np.zeros_like(coordinates).astype(float)
    interpolation_coordinates[:, 0] = coordinates[:, 0] * d_interpolation_step_size
    interpolation_coordinates[:, 1] = coordinates[:, 1] * h_interpolation_step_size
    interpolation_coordinates[:, 2] = coordinates[:, 2] * w_interpolation_step_size

    mapping_coordinates = np.vstack((
        np.squeeze(interpolation_coordinates[:, 0]),
        np.squeeze(interpolation_coordinates[:, 1]),
        np.squeeze(interpolation_coordinates[:, 2])
    ))

    new_vol = np.zeros((new_depth, new_height, new_width), dtype=volume.dtype)

    mapped_values = map_coordinates(volume, mapping_coordinates, order=order)

    new_vol[coordinates[:, 0], coordinates[:, 1], coordinates[:, 2]] = mapped_values
    return new_vol


def resize_volume(volume, scale, order=3):
    """
    Resizes a volume with a given scale using B-Splines. Uses ndimage, not ram intense
    If you need floating point precision, change the dtype of the volume before resizing!

    Args:
        volume (np.ndarray):
        scale:
        order (int): order of spline interpolation; 1 to 5; 3 is recommended
            Creating lower degree curves results in faster performance during subsequent operations such as
            machining, display, etc. Using higher degree curves reduces the chance of transferring data to other
            systems which may not support them. Also, the creation of a high degree curve through many points may
            lead to unpredictable results.

    Interpolation method:
        Example in 1d: suppose an array with 2 values.
            the indexes are then [0, 1]
            suppose scale = 2
            the target indexes are then [0, 1, 2, 3]
            The values are mapped like:
            [0,       1]
            [0, 1, 2, 3]  -> indexes 1 and 2 are interpolated
        interpolation function is B-Spline with a given order (set by user)

    Returns: resized volume

    """
    new_vol = zoom(volume, scale, order=order, mode='grid-constant')

    return new_vol


if __name__ == '__main__':
    timer = timeit.Timer(
        "resize_volume_scipy(np.ones((100, 100, 100), dtype=np.uint8), 2)",
        "import numpy as np; from scipy.ndimage import zoom; from __main__ import resize_volume_scipy"
    )
    result = timer.timeit(10)
    print(f'Resizing of a 100x100x100 volume to 200x200x200 takes around: {round(result)} seconds.')
