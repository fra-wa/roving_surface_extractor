import cc3d
import logging
import numpy as np
import os

import trimesh
from skimage import measure

from ..file_handling.cluster_reading import load_gray_volume
from ..file_handling.general_writing import save_volume
from ..file_handling.utils import create_folder


def get_non_empty_volume_region(volume, only_start=False, only_end=False):
    d, h, w = volume.shape
    d_start = 0
    d_end = d
    h_start = 0
    h_end = h
    w_start = 0
    w_end = w

    logging.info('Getting min and max dimensions of the labelled area')
    if not only_end and not only_start:
        for z in range(d):
            if volume[z].any():
                d_start = z
                break
        for y in range(h):
            if volume[:, y].any():
                h_start = y
                break
        for x in range(w):
            if volume[:, :, x].any():
                w_start = x
                break

        for x in reversed(range(w)):
            if volume[:, :, x].any():
                w_end = x + 1
                break
        for y in reversed(range(h)):
            if volume[:, y].any():
                h_end = y + 1
                break
        for z in reversed(range(d)):
            if volume[z].any():
                d_end = z + 1
                break
    elif only_end and not only_start:
        for z in range(d):
            if volume[z].any():
                d_start = z
                break
        for y in range(h):
            if volume[:, y].any():
                h_start = y
                break
        for x in range(h):
            if volume[:, :, x].any():
                w_start = x
                break
    elif only_start and not only_end:
        for x in reversed(range(h)):
            if volume[:, :, x].any():
                w_end = x + 1
                break
        for y in reversed(range(h)):
            if volume[:, y].any():
                h_end = y + 1
                break
        for z in reversed(range(d)):
            if volume[z].any():
                d_end = z + 1
                break

    return d_start, d_end, h_start, h_end, w_start, w_end


def get_or_create_labelled_volume(volume, connection, base_folder, k_largest_objects=5, logger=None):
    """

    Args:
        volume: initial volume of gray values
        connection: for connected component analysis, defaults to 26, can be 6, 18 or 26
        base_folder: folder of the volume to analyze
        k_largest_objects: check only the k largest objects
        logger: the logging instance

    Returns:
    """

    if volume is None:
        volume = load_gray_volume(base_folder, logger)

    d, h, w = volume.shape
    d_start, d_end, h_start, h_end, w_start, w_end = get_non_empty_volume_region(volume)
    volume = volume[d_start:d_end, h_start:h_end, w_start:w_end]

    if k_largest_objects is not None and k_largest_objects >= 1:
        logging.info(f'Getting {k_largest_objects} largest connected components. This may take some time!')
        volume, labels = cc3d.largest_k(
            volume,
            k=k_largest_objects,
            connectivity=connection,
            delta=0,
            return_N=True,
        )
    else:
        logging.info('Getting connected components. This may take some time!')
        volume, labels = cc3d.connected_components(volume, connectivity=connection, return_N=True, max_labels=2**32 - 1)

    if k_largest_objects is not None:
        labels = np.max(volume)
    if labels < 2**8:
        volume = volume.astype(np.uint8)

    volume = np.pad(volume, pad_width=((d_start, d - d_end), (h_start, h - h_end), (w_start, w - w_end)))
    labels = list(range(1, labels + 1))
    return volume, labels


def start_extraction(segmented_volume_folder, max_labels_to_check, save_normals, logger=None):
    volume, labels = get_or_create_labelled_volume(
        volume=None,
        connection=26,
        base_folder=segmented_volume_folder,
        k_largest_objects=max_labels_to_check,
        logger=logger,
    )
    out_folder = os.path.join(segmented_volume_folder, 'extracted_clusters')
    create_folder(out_folder)
    for label in labels:
        logging.info(f'Progress {label}/{len(labels)}: Extracting cluster')
        cluster = volume == label
        cluster = cluster.astype(np.uint8)
        cluster[cluster != 0] = 255
        logging.info(f'Progress {label}/{len(labels)}: Saving cluster')
        cluster_name = 'cluster_' + str(label).zfill(len(str(len(labels))))
        current_out_folder = os.path.join(out_folder, cluster_name)
        create_folder(current_out_folder)
        binary_volume_dir = os.path.join(current_out_folder, 'binary_volume')
        save_volume(cluster, binary_volume_dir)
        try:
            # use smaller volume for marching cubes
            d_start, d_end, h_start, h_end, w_start, w_end = get_non_empty_volume_region(cluster)
            cluster = cluster[d_start:d_end, h_start:h_end, w_start:w_end]
            # need padding to get continuous surface
            cluster = np.pad(cluster, pad_width=1)

            logging.info(f'Progress {label}/{len(labels)}: Extracting surface')
            vertices, faces, normals, values = measure.marching_cubes(cluster, 0)
            del values

            offset_d = d_start - 1  # -1 cause of padding
            offset_h = h_start - 1
            offset_w = w_start - 1
            offset = np.array([offset_d, offset_h, offset_w], dtype=vertices.dtype)
            vertices += offset
            # Trimesh expects: x, y, z
            vertices = np.fliplr(vertices)
            faces = np.fliplr(faces)
            normals = np.fliplr(normals)

            if save_normals:
                surface_path = os.path.join(current_out_folder, 'surface.stl')
            else:
                surface_path = os.path.join(current_out_folder, 'surface.ply')
            mesh = trimesh.Trimesh(
                vertices=vertices, faces=faces, vertex_normals=normals
            )
            logging.info(f'Progress {label}/{len(labels)}: Saving surface')
            if save_normals:
                mesh.export(surface_path)
            else:
                encoding_kwargs = {'encoding': 'binary'}
                mesh.export(surface_path, **encoding_kwargs)

        except MemoryError as e:
            logging.info(f'Not enough ram. Cannot create surface.')
